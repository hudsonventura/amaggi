<?php


class adicionarLink extends Core\ModularController{
	public function __construct() {
		parent::__construct();
	}
    
    public function index(){
	$this->coreView['title']='Adicionar Link';

	$this->loadModel('modelLink', 'modelLink');
	$this->coreView['listaLinks'] = $this->models->modelLink->getLinks();
	$this->coreView['geral'] = $this->models->modelLink->getVpnAutoGeral();
	
	if($_POST){
	    $this->models->modelLink->salvarLink($_POST);
	}
	
	$this->loadView('viewAdicionarLink');
    }
}
