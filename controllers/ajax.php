<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');

class ajax extends ModularCore\Controller{

    //private $rotaPadrao = '172.20.0.5';
    private $host1 = '172.12.11.251';
    private $host2 = '172.12.11.252';
    private $port = '23';
    private $user = 'admin.core';
    private $pass = '@R1FY0cDsk';




	function __construct() {
			parent::__construct();
			$this->loadModel('modelLink', 'modelLink');
			$this->loadModel('modelLog', 'modelLog');
			$this->loadLib('session');

			$this->loadLib('Mail');

			//$this->libs->Mail->AddAddress('suporte.infra@amaggi.com.br', 'Helpdesk');
			//$this->libs->Mail->AddAddress('csc@amaggi.com.br', 'Centro de Serviço Compartilhado');
			$this->libs->Mail->AddAddress('hudson.ventura@amaggi.com.br', 'Centro de Serviço Compartilhado');
			$this->libs->Mail->AddAddress('carlos.kempa@amaggi.com.br', 'Centro de Serviço Compartilhado');

			if(isset($_GET['json'])){
					$this->json = $_GET['json'];
			}
    }


    public function setOpcoes($array = null){

		if(!isset($array)){
			$array = $this->json;
		}
		if(!$array['sender']){
			$array['sender'] = 'SISTEMA DE LINKS';
		}

		

		$link = $this->models->modelLink->getLink($array['campo']);
		$return = $this->models->modelLink->setVpnAuto($this->json['campo'], $this->json['valor']);
		if($array['valor'] == 'true'){
			$log = $link['titulo'].' - Colocado modo automático';
			$return = $this->models->modelLog->write($log, $this->json['sender']);
		}else{
			$log = $link['titulo'].' - Colocado modo manual';
			$return = $this->models->modelLog->write($log, $this->json['sender']);
		}
		echo json_encode(array("return"=>"$return"));
	}

    public function getTempo(){
	    echo json_encode($this->models->modelLink->getTempo());
    }

		public function comutaVPN($array = null){


			if(!isset($array)){
				$array = $this->json;
			}

			

			//var_dump($array);
			$array = $array['json'];

			if(!isset($array['sender'])){
				$array['sender'] = 'Sistema de Monitoramento de Links';
			}
			$comandos = array($this->user, $this->pass,'en',$this->user, $this->pass, 'configure terminal');
			if(isset($array)){
					
					$link = $this->models->modelLink->getLink($array['campo']);

					if($link['vpnAuto'] == 1){
						$vpnAuto = 'Automático';
					}else{
						$vpnAuto = 'Manual';
					}

					if($array['valor'] == 'true'){

					$this->models->modelLink->ativarVPN($array['campo']);
					$executar = array_merge($comandos, explode('|', $link['cmd_ativaVPN']));

					$log = $link['titulo'].' - VPN ativada. Status da automação: '.$vpnAuto;

					$ativa = 'ativada';
					if($vpnAuto == 'Automático'){
						$this->libs->Mail->Body = '<br /><br />A VPN de <b>'.$link['titulo'].'</b> foi <b>ativada</b> por <b>'.$array['sender'].'</b>.<br/>Status da automação: '.$vpnAuto;
					}else{

					}


					if($vpnAuto == 'Automático'){
						$usuario = 'Sistema de Monitoramento de Links';
					}else{
						$usuario = $array['sender'];
					}


					$return = true;
					}else{
					$this->models->modelLink->desativarVPN($array['campo']);

					$executar = array_merge($comandos, explode('|', $link['cmd_desativaVPN']));

					$log = $link['titulo'].' - VPN desativada. Status da automação: '.$vpnAuto;

					$ativa = 'desativada';
					if($vpnAuto == 'Automático'){
						$usuario = 'Sistema de Monitoramento de Links';
					}else{
						$usuario = $array['sender'];
					}




					$return = false;
					}

					$this->libs->Mail->Subject  = "VPN de ".$link['titulo']." desativada por $usuario";
					$this->libs->Mail->Body =
													'<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
													<html>
														<head>
															<meta http-equiv="Content-Type" content="text/html;UTF-8" />
														</head>
														<body style="margin: 0px; background-color: #F4F3F4; font-family: Helvetica, Arial, sans-serif; font-size:12px;" text="#444444" bgcolor="#F4F3F4" link="#21759B" alink="#21759B" vlink="#21759B" marginheight="0" topmargin="0" marginwidth="0" leftmargin="0">
															<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F4F3F4">
																<tbody>
																	<tr>
																		<td style="padding: 15px;"><center>
																			<table width="100%" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
																				<tbody>
																					<tr>
																						<td align="left">
																							<div style="border: solid 1px #d9d9d9;">
																								<table id="header" style="line-height: 1.6; font-size: 12px; font-family: Helvetica, Arial, sans-serif; border: solid 1px #FFFFFF; color: #444;" border="0" width="93%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
																									<tbody>
																										<tr>
																											<td style="color: #ffffff;" colspan="2" valign="bottom" height="30">.</td>
																										</tr>
																										<tr>
																											<td style="line-height: 32px; padding-left: 30px;" valign="baseline"><span style="font-size: 32px;"><a style="text-decoration: none;" href="https://suporte.amaggi.com.br" target="_blank"><img class="alignnone size-full wp-image-21" src="https://suporte.amaggi.com.br/repositorios/imagens/csc/logo_csc.png" alt="Logo-CSC" /></a></span></td>
																											<td style="padding-right: 30px;" align="right" valign="baseline"><span style="font-size: 14px; color: #777777;">Sistema de Monitoramento de Links</span></td>
																										</tr>
																									</tbody>
																								</table>
																								<table id="content" style="margin-top: 15px; padding-left: 30px; color: #444; line-height: 1.6; font-size: 12px; font-family: Arial, sans-serif;" border="0" width="93%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
																									<tbody>
																				<tr>
																					<td colspan="2">
																						<div style="padding: 15px; border-top: solid 1px #d9d9d9;"><p><h2>VPN de '.$link['titulo'].' '.$ativa.'</h2><br /><br />
																							A VPN de '.$link['titulo'].' foi '.$ativa.' por <b>'.$usuario.'</b><br /><br />


																							<br />
																							<div style="border-top: solid 1px #d9d9d9;width: 100%;font-size: 11px;color: #999999;font-family: arial,sans-serif;"><br />
																								Email enviado de forma automática</p>
																								<p>
																									Para acessar o sistema e verificar o estado atual, visite: <a href="https://suporte.amaggi.com.br" rel="nofollow">https://suporte.amaggi.com.br</a><br />
																								</p>
																							</div>
																							<br />
																						</div>
																					</td>
																				</tr>
																									</tbody>
																								</table>

																							</div>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																			</center>
																</td>
																	</tr>
																</tbody>
															</table>
														</body>
													</html>';

					$resultadoEmail = $this->libs->Mail->Send();
					//var_dump($resultadoEmail);

					$this->loadLib('Telnet');
					$executar = array_merge($executar, array('write memory', 'SAIR'));
				//if(ModularCore\core::$coreConfig['environment'] == 'PRD'){
					$return1 = $this->libs->Telnet->sendCommands($this->host1, $this->port, $executar);
					$return2 = $this->libs->Telnet->sendCommands($this->host2, $this->port, $executar);
				//}else{
				//	$return1 = json_encode(array('Executando em '.$this->host1.' os seguintes comandos: ', $executar));
				//	$return2 = json_encode(array('Executando em '.$this->host2.' os seguintes comandos: ', $executar));
				//}


					$return = $this->models->modelLog->write($log, $array['sender']);
					$retornoArray = array($log, $this->removeSpecialChar(substr($return1, 6)));
					$retornoJson = json_encode($retornoArray);
					die($retornoJson);
			}
    }




}