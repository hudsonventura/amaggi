<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
require_once('controllerPrincipal.php');

class BibliotecaGerencia extends controllerPrincipal{
	
	
	
	
	function __construct() {
		
		parent::__construct($this);
		$this->loadModel('modelBiblioteca', 'model');
		
		//Permissoes
		$this->loadLib('Permission');
		$this->libs->Permission->required = array('csc.processo');
		$this->libs->Permission->verify($this->coreView['usuario']['memberof']);
		//Permissoes
	}


    
   public function index(){
		
		//trata a busca
		$busca = '';
		if(isset($_GET['busca'])){
			$busca = $_GET['busca'];
		}
		
		$this->coreView['title']='Lista de Conhecimentos';
		$this->loadLib('make');
		$this->coreView['geral'] = $this->model->getListaConhecimentos($busca);
		
		$this->coreView['lista'] = $this->model->getLinks();
		
		var_dump($this->coreView['lista']);
		
		$this->coreView['table'] = $this->make->table(array('Localidade', 'Ip do Roteador', 'Porta', 'Timeout', 'Comutação Automática da VPN', 'Virada', 'Editar', 'Deletar'), $this->coreView['lista'], 'table table-bordered');
		
		$this->loadView('viewLinkslistar');
    }
    

    
    
}