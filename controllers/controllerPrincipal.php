<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');


class controllerPrincipal extends ModularCore\Controller{
	function __construct() {
		parent::__construct();

		$this->loadLib('session');
		$this->loadLib('activedirectory');


		$login = $this->libs->session->getData('login');

		$directLogin = isset($_GET['directLogin']);
		$directPass = isset($_GET['directPass']);

		if(!$login && !$directLogin && !$directPass){
			$this->loadModel('ModelUsuario');
			$userSE = $this->models->ModelUsuario->getUserSE($_SERVER['REMOTE_ADDR']);

			if($userSE){
				$this->loadLib('activedirectory');
				$users = $this->libs->activedirectory->getUser('samaccountname='.$userSE);
				foreach ($users as $key => $usuario) {
					$user = $usuario;
					break;
				}
				$user = $this->models->ModelUsuario->getUserHCM($user);
				//var_dump($user);
				$this->libs->session->setData('login', $user['samaccountname'][0]);
				$this->libs->session->setData('usuario', $user);
				//header('Location: '.$this->module.'/'.$this->module.'/'.$this->function);
			}else{
				header('Location: '.MODULEDIR.'/login');
			}


		}
		$this->loadLib('console');
		$this->libs->console->write('Logado como '.$login);

		if(!$login && $directLogin && $directPass){
			$authenticate = $this->libs->activedirectory->authenticate($_GET['directLogin'], $_GET['directPass']);
			if(!$authenticate){
				header('Location: '.MODULEDIR.'/login');
			}
		}

		$this->usuario = $this->libs->session->getData('usuario');
		$this->coreView['usuario'] = $this->usuario;
	}

}
