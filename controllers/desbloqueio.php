<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');



class Desbloqueio extends ModularCore\Controller {

	function __construct() {
		parent::__construct();
		$this->loadLib('session');
		$this->loadModel('modelLog', 'modelLog');

		if($this->libs->session->getData('login')){
			header('Location: '.MODULEDIR.'/home');
		}
	}

	public function index(){

	$this->loadLib('activedirectory');
	$this->coreView['title'] = 'Desbloquear usuário de rede e email - Amaggi';

	$this->loadModel('modelLink');


	if(isset($_POST['cpf'])){
		$this->loadLib('activedirectory');

		$users = $this->libs->activedirectory->getUser('description='.$_POST['cpf'], 'all');
		
	    if($users){

			foreach ($users as $key => $user) {
				$unlocked = $this->libs->activedirectory->unblockUser($user, $user['domain']);
				if($unlocked){
					$this->models->modelLog->write('Conta desbloqueada. Usuário: '.$user['samaccountname'][0], 'SISTEMA DE USUÁRIOS');
					$this->libs->session->setTemp('msgLogin', 'Usuário desbloqueado com sucesso!');
					$this->coreView['msgType']= 'success';
					$this->coreView['msgDesbloqueio'] = $this->libs->session->getTemp('msgLogin');
				}else{
					$this->models->modelLog->write('Usuário com acesso desativado ou com acesso administrativo. Usuário: '.$user['samaccountname'][0].' | IP: '.$_SERVER['REMOTE_ADDR'], 'SISTEMA DE USUÁRIOS');
					$this->libs->session->setTemp('msgLogin', 'Houve um erro ao tentar desbloquear seu usuario. Por favor entre em contato no ramal 5050');
					$this->coreView['msgType']= 'danger';
					$this->coreView['msgDesbloqueio'] = $this->libs->session->getTemp('msgLogin');
				}
			}
	    }else{
			$this->models->modelLog->write('Usuário digitou um documento errado. Documento: '.$_POST['cpf'].' | IP: '.$_SERVER['REMOTE_ADDR'], 'SISTEMA DE USUÁRIOS');
			$this->libs->session->setTemp('msgLogin', 'Este CPF não consta nem nossa base de dados. Por favor tente novamente.');
			$this->coreView['msgType']= 'danger';
			$this->coreView['msgDesbloqueio'] = $this->libs->session->getTemp('msgLogin');
	    }

	}


	$this->loadView('viewLoginDesbloqueio');
    }


}