<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');



class Error404 extends ModularCore\Controller {

	function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->coreView['title'] = 'Página não encontrada - Error 404';
		
		$this->loadLib('make');
		$this->coreView['home'] = $this->libs->make->link('<span class="glyphicon glyphicon-home"></span> Ir para o início', '', 'btn btn-default btn-lg');
		$this->coreView['back'] = $this->libs->make->link('<span class="fa fa-undo"></span> Voltar página', 'javascript:window.history.go(-1)', 'btn btn-primary btn-lg');
		
		$this->loadView('error404');
		
	}
}