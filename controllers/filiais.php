<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
require_once('controllerPrincipal.php');

class Filiais extends ModularCore\Controller{

	function __construct() {
		parent::__construct();
		$this->loadModel('modelLink', 'modelLink');
	}


	public function index(){
	$this->coreView['title']='Monitoramento de Links de Filiais';
	
	$this->coreView['listaLinks'] = $this->models->modelLink->getLinks();
	//$this->coreView['geral'] = $this->models->modelLink->getGeral();
	$tempo = time() - $this->models->modelLink->getTempo();
	if($tempo < 240){
		if($tempo<60){
			$ultimaVerificacao = 'Última verificação há '.$tempo.'s atrás.';
		}else{
			$ultimaVerificacao = 'Última verificação há '.floor($tempo/60).'m atrás.';
		}
	}else{
		$ultimaVerificacao = 'Última verificação há '.floor($tempo/60).'m'.' atrás.';
		if($tempo > 300){
			$ultimaVerificacao = 'Verificando disponibilidade de links';
			$this->loadModel('modelLog', 'modelLog');
			//$this->models->modelLog->write('A interface do servidor não está ativa.', $this->usuario['samaccountname'][0]);
			$this->coreView['admin'] = ''; // essa é a informação que a view precisa para ativart o JS que faz a verificação
			$this->models->modelLink->gravaTempo();
		}
	}
	$this->coreView['ultimaVerificacao'] = $ultimaVerificacao;
	
	$this->loadView('viewFiliais');
	echo "<meta HTTP-EQUIV='refresh' CONTENT='30;URL=''>";
    }
    
}