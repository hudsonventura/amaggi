<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
require_once('controllerPrincipal.php');

class Links extends controllerPrincipal{
	
	
	
	
	function __construct() {
		
		parent::__construct($this);
		$this->loadModel('modelLink', 'modelLink');
		
		//Permissoes
		$this->loadLib('Permission');
		$this->libs->Permission->required = array('csc.processo', 'GU_TI_SCSM_Perm_SuporteInfra', 'GU_CRP_ACL_GestaoTI', 'GU_TI_Ger_Firewall', 'GU_CRP_ACL_Infra');
		$this->libs->Permission->verify($this->coreView['usuario']['memberof']);
		//Permissoes
	}


	public function monitoramento(){
		$this->coreView['title']='Monitoramento e Comutação de Links';
		$this->coreView['listaLinks'] = $this->models->modelLink->getLinks();
		
		$now = new DateTime(Date('Y-m-d H:i:s'));
		$date = new DateTime($this->models->modelLink->getTempo());
		
		$tempo = $now->getTimestamp() - $date->getTimestamp();//+3600;
		
		$ultimaVerificacao = 'Última verificação há '.floor($tempo/60).'m atrás.';
		if($tempo < 120){
			if($tempo>5)
				$ultimaVerificacao = 'Última verificação há <b>'.$tempo.'s</b> atrás, às <b>'.$date->format('H:i d/m/Y').'</b>';
			else
				$ultimaVerificacao = 'Última verificação <b>exatamente agora</b>, às <b>'.$date->format('H:i d/m/Y').'</b>';
		}else{
			$ultimaVerificacao = '<b style="color: red">Última verificação há '.floor($tempo/60).'m'.' atrás.</b>';
			if($tempo > 300){
				$this->coreView['title']='<b style="color: red">MONITOR DE SERVIÇOS DESATIVADO.</b><br><small>Favor verificar o serviço "ServicoMonitor" do SRVVM190</small>';
			}
		}
		$this->coreView['ultimaVerificacao'] = $ultimaVerificacao;
		
		$this->loadView('viewLinksComutacao');
		echo "<meta HTTP-EQUIV='refresh' CONTENT='30;URL=''>";
    }
    
	public function alterar(){
		$this->coreView['title']='Alteração do Cadastro do Link';
		$this->coreView['geral'] = $this->models->modelLink->getGeral();

		
		if(isset($_GET)){
			$link = $this->models->modelLink->getLink($_GET['id']);
			$cmd_ativaVPN = $link['cmd_ativaVPN'];
			$cmd_ativaVPN = str_replace('|',"\n", $cmd_ativaVPN);
			$link['cmd_ativaVPN'] = $cmd_ativaVPN;
			
			$cmd_desativaVPN = $link['cmd_desativaVPN'];
			$cmd_desativaVPN = str_replace('|',"\n", $cmd_desativaVPN);
			$link['cmd_desativaVPN'] = $cmd_desativaVPN;
			
			$this->coreView['link'] = $link;
		}
		
		if($_POST){
				$link = $_POST;
				$cmd_ativaVPN = $link['cmd_ativaVPN'];
				$cmd_ativaVPN = explode("\n ", $cmd_ativaVPN);
				$cmd_ativaVPN = implode("\n", $cmd_ativaVPN);
				$cmd_ativaVPN = explode("\n", $cmd_ativaVPN);
				//var_dump($_POST); die();
				$this->models->modelLink->update($_POST);
				header('Location: gerenciar');
		}
		
		$this->loadView('viewLinksAdicionar');
    }
    
	public function deletar(){
		$this->coreView['title']='Deletar Cadastro do Link';

		
		if(isset($_GET)){
			$link = $this->models->modelLink->getLink($_GET['id']);
			$this->models->modelLink->delete($link);
		}
		
		
		header('Location: '.MODULEDIR.'/links/gerenciar');
    }
    
    
    
	public function adicionar(){
		$this->coreView['title']='Adicionar Links';

		$this->coreView['listaLinks'] = $this->models->modelLink->getLinks();
		$this->coreView['geral'] = $this->models->modelLink->getGeral();
		if($_POST){
			$array = $_POST;
			$array['tipo_servico'] = 0;
				$this->models->modelLink->salvarLink($array);
			header('Location: '.MODULEDIR.'/links/gerenciar');
		}
		
		$this->loadView('viewLinksAdicionar');
    }
    
   public function index(){
    	$this->coreView['title']='Dashboard';
    	
    	
    	$this->loadModel('modelLog', 'modelLog');
    	$this->loadModel('modelLink', 'modelLink');
    	
    	//usuarios
		$relatorioUsuarios = array();
		$relatorioUsuarios['Auto desbloqueios'] = $this->models->modelLog->count('Conta desbloqueada.');
		$relatorioUsuarios['Desbloqueios pela equipe'] = $this->models->modelLog->count('DESBLOQUEOU');
		$relatorioUsuarios['Reset de senha pela equipe'] = $this->models->modelLog->count('RESETOU ');
		$this->coreView['relatorioUsuarios'] = $relatorioUsuarios;
		
		//links
		$relatorioLinks = array();
		$links = $this->models->modelLink->getLinks();
		foreach($links as $link){
			$relatorioLinks[$link['titulo']] = $this->models->modelLog->count($link['titulo'].' está operando normalmente');
		}
		arsort($relatorioLinks);
		$this->coreView['relatorioLinks'] = $relatorioLinks;
		
		$now = new DateTime(Date('Y-m-d H:i:s'));
		$date = new DateTime($this->models->modelLink->getTempo());
		$tempo = $now->getTimestamp() - $date->getTimestamp()+3600;
		$ultimaVerificacao = 'Última verificação há '.floor($tempo/60).'m atrás.';
		if($tempo < 120){
			if($tempo>5)
				$ultimaVerificacao = 'Última verificação há <b>'.$tempo.'s</b> atrás, às <b>'.$date->format('H:i d/m/Y').'</b>';
			else
				$ultimaVerificacao = 'Última verificação <b>exatamente agora</b>, às <b>'.$date->format('H:i d/m/Y').'</b>';
		}else{
			$ultimaVerificacao = '<b style="color: red">Última verificação há '.floor($tempo/60).'m'.' atrás.</b>';
			if($tempo > 300){
				$this->coreView['title']='<b style="color: red">MONITOR DE SERVIÇOS DESATIVADO.</b><br><small>Favor verificar o serviço "ServicoMonitor" do SRVVM190</small>';
			}
		}
		$this->coreView['ultimaVerificacao'] = $ultimaVerificacao;
		$this->coreView['tempo'] = $tempo;
		
		//links offline
		$relatorioOffline = array();
		foreach($links as $link){
			if($link['statusLink'] == 0){
				$relatorioOffline[$link['titulo']] = '';
			}
		}
		$this->coreView['relatorioOffline'] = $relatorioOffline;
		
		//VPNs offline
		$relatorioVPN = array();
		foreach($links as $link){
			if($link['statusVPN'] == 1){
				$relatorioVPN[$link['titulo']] = '';
			}
		}
		$this->coreView['relatorioVPN'] = $relatorioVPN;
		
		//tempos
		
		$time = time() - $this->models->modelLink->getTempo();
		$tempo = $time.'s';
		if($time > 60){
			$tempo = floor($time/60).'m';
		}
		if($time > 3600){
			$tempo = floor($time/3600).'h';
		}
		$ultimaVerificacao = date('H:i d/m/Y', strtotime($this->models->modelLink->getTempo()));
		
		
		$this->loadView('viewDashboard', get_defined_vars());
    }
    

    
    public function gerenciar(){
    	
    	//Permissoes
		$this->libs->Permission->required = array('GU_CRP_ACL_Infra', 'csc.processo', 'GU_TI_Ger_Firewall');
		$this->libs->Permission->verify($this->coreView['usuario']['memberof']);
		//Permissoes
		
		
		$this->coreView['title']='Lista de Links';
		$this->loadLib('make');
		$this->coreView['geral'] = $this->models->modelLink->getGeral();
		
		$listaNova = array();
		$lista = $this->models->modelLink->getLinks();
		foreach($lista as $link){
			unset($link['statusLink']);
			unset($link['tipo_servico']);
			unset($link['statusVPN']);
			unset($link['mensagem']);
			unset($link['aguardando']);
			unset($link['ip_alvo']);
			unset($link['cmd_ativaVPN']);
			unset($link['cmd_desativaVPN']);
			//VIRADA AUTOMATIRA?
				if($link['vpnAuto']) $link['vpnAuto'] = 'Sim'; else $link['vpnAuto'] = null;
				
				//VIRADA INVERSA?
				if($link['tipo_virada']) $link['tipo_virada'] = 'Inversa'; else $link['tipo_virada'] = null;
			
			
			$link['edicao'] = $this->libs->make->link('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', MODULEDIR.'/links/Alterar?id='.$link['id'], 'btn btn-success');
			$link['delete'] = $this->libs->make->link('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', MODULEDIR.'/links/Deletar?id='.$link['id'], 'btn btn-danger');
			unset($link['id']);
			array_push($listaNova, $link);
		}

	
		$this->coreView['listaLinks'] = $listaNova;
		$this->coreView['table'] = $this->libs->make->table(array('Localidade', 'Ip do Roteador', 'Porta', 'Timeout', 'Comutação Automática da VPN', 'Virada', 'Editar', 'Deletar'), $this->coreView['listaLinks'], 'table table-bordered');
		
		$this->loadView('viewLinkslistar');
    }
    
    public function logs(){
    	$this->loadLib('Pagination');
		$this->loadLib('make');
		$this->coreView['title']='Logs de Links';
		
		$page = 1;
		if(isset($_GET['Page'])){
			$page = $_GET['Page'];
		}
		
		$busca= '';
		if(isset($_GET['busca'])){
			$busca = $_GET['busca'];
		}
		$divisao = 100;
		$logs = $this->models->modelLink->getLogsLink($page, $divisao, $busca);
		$total = $this->models->modelLink->countLogsLinks($busca);
		$novoLogs = array();
		if(is_array($logs)){
			foreach($logs as $log){
			    unset($log['id']);
			    array_push($novoLogs, $log);
			}
		}else{
			$this->coreView['msgLog']= 'Sua busca não retornou resultados';
			$this->coreView['msgLogType']= 'info';
		}
		
		
		
		$this->coreView['logs']= $this->libs->make->table(array('Horário', 'Localidade', 'Ping Min (ms)', 'Ping Mid', 'Ping Max', 'Ação'), $novoLogs, 'table table-striped');
		$this->coreView['pagination'] = $this->libs->Pagination->render($total, $page, $divisao, 10, FUNCTIONDIR."?busca=$busca&Page=", 'Primeira', 'Ant.', 'Prox.', 'Última');
		
		
		$this->loadView('viewLogs');
	}
    
    
}