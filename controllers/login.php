<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');



class Login extends ModularCore\Controller {

	function __construct() {
		parent::__construct();
		$this->loadLib('session');
		$userLogin = $this->libs->session->getData('login');
		if($userLogin){
			header('Location: home');
		}else{
			$this->loadModel('ModelUsuario');
			$userSE = $this->models->ModelUsuario->getUserSE($_SERVER['REMOTE_ADDR']);

			if($userSE){
				$this->loadLib('activedirectory');
				$user = $this->libs->activedirectory->getUser('samaccountname='.$userSE);
				$user = $this->models->ModelUsuario->getUserHCM($user);
				//var_dump($user);
				$this->libs->session->setData('login', $user['samaccountname'][0]);
				$this->libs->session->setData('usuario', $user);
				header('Location: home');
			}
		}
	}

	public function index(){
	$this->loadLib('activedirectory');
	$this->coreView['title'] = 'Login';

	$this->loadModel('modelLink', 'modelLink');
	$this->loadLib('make');
	$links = $this->models->modelLink->getLinks();

	//var_dump($this->models->modelLink);

	$linksFora = array();
	foreach($links as $link){
		if($link['statusLink'] == 0){
			unset($link['id']);
			unset($link['vpnAuto']);
			unset($link['ip_alvo']);
			unset($link['statusVPN']);
			unset($link['statusLink']);
			unset($link['aguardando']);
			unset($link['cmd_desativaVPN']);
			unset($link['cmd_ativaVPN']);
			unset($link['mensagem']);
			array_push($linksFora, $link);
		}
	}




	if(isset($_POST['login']) && isset($_POST['pass']) ){
		//TRY TO LOGIN
		if(isset($_POST['login']) && isset($_POST['pass'])){
			if($this->libs->activedirectory->authenticate($_POST['login'], $_POST['pass'])){
				$users = $this->libs->activedirectory->getUser('samaccountname='.$_POST['login'], 'all');
				
				foreach ($users as $key => $user) {
					$user = $this->models->ModelUsuario->getUserHCM($user);
					break;
				}
				

				
				if(isset($user)){//['memberof']
					$this->loadModel('modelUsuario');
					$get = $this->models->modelUsuario->get($_POST);

						$this->libs->session->setData('login', $_POST['login']);
						$this->libs->session->setData('usuario', $user);
						header('Location: home');
				}else{
					$this->coreView['msgLogin']=  'Você não possui acesso a esta ferramenta.';
					$this->coreView['msgType']= 'danger';
				}
			}else{
				$this->coreView['msgLogin']= 'Usuário de rede ou senha não foram digitados corretamente. Tente novamente.';
				$this->coreView['msgType']= 'danger';
			}
		}
	}

	if(isset($_POST['cpf'])){
	    $this->loadLib('activedirectory');
	    $user = $this->libs->activedirectory->getUser('description='. $_POST['cpf']);
	    //var_dump($user);
	    if($user){
		if($user['accountexpires'][0] == 0){
		    $this->libs->session->setTemp('msgLogin', 'Sua senha expirou. Você não trocou sua senha dentro dos 30 dias. Contate o Helpdesk para renovar a sua senha.');
		    $this->coreView['msgType']= 'warning';
		    $this->coreView['msgDesbloqueio'] = $this->libs->session->getTemp('msgLogin');
		}else{
		    if($this->libs->activedirectory->unblockUser($user, $user['domain'])){
			$this->libs->session->setTemp('msgLogin', 'Usuário desbloqueado com sucesso!!!');
			$this->coreView['msgType']= 'success';
			$this->coreView['msgDesbloqueio'] = $this->libs->session->getTemp('msgLogin');
		    }else{
			$this->libs->session->setTemp('msgLogin', 'Impossível desbloquear seu usuário pois seu usuário possui acesso administrativo. Somente o Helpdesk pode desbloquear a sua senha');
			$this->coreView['msgType']= 'danger';
			$this->coreView['msgDesbloqueio'] = $this->libs->session->getTemp('msgLogin');
		    }

		}
	    }else{
		$this->libs->session->setTemp('msgLogin', 'Este CPF não consta nem nossa base de dados. Por favor tente novamente.');
		$this->coreView['msgType']= 'danger';
		$this->coreView['msgDesbloqueio'] = $this->libs->session->getTemp('msgLogin');
	    }

	}


	$this->loadView('viewLogin');
    }
}