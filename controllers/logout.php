<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');



class Logout extends ModularCore\Controller {

	function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->loadLib('session');
		$this->libs->session->cleanData('login');
		header('Location: '.MODULEDIR.'/login');
	}
}