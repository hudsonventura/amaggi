<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
require_once('controllerPrincipal.php');


class logs extends controllerPrincipal{

    function __construct() {
		parent::__construct();
		$this->loadModel('modelLog', 'modelLog');
		$this->loadLib('Pagination');

		//Permissoes
		$this->loadLib('Permission');
		$this->libs->Permission->required = array('GU_TI_SCSM_Perm_SuporteInfra' , 'csc.processo', 'GU_CRP_ACL_GestaoTI', 'corporate.security');
		$this->libs->Permission->verify($this->coreView['usuario']['memberof']);
		//Permissoes o
    }

    public function index(){
	$this->loadLib('make');
	$this->coreView['title']='Logs';

	$divisao= 100;

	$page = 1;
	if(isset($_GET['Page'])){
		$page = $_GET['Page'];
	}

	$busca= '';
	if(isset($_GET['busca'])){
		$busca = $_GET['busca'];
	}

	$logs = $this->models->modelLog->get($page, $divisao, $busca);
	$total = $this->models->modelLog->count($busca);
	$novoLogs = array();
	if(is_array($logs)){
		foreach($logs as $log){
		    unset($log['id']);
		    array_push($novoLogs, $log);
		}
	}else{
		$this->coreView['msgLog']= 'Sua busca não retornou resultados';
		$this->coreView['msgLogType']= 'info';
	}



	$this->coreView['logs']= $this->libs->make->table(array('Horário', 'Usuário', 'IP', 'Ação'), $novoLogs, 'table table-striped');
	$this->coreView['pagination'] = $this->libs->Pagination->render($total, $page, $divisao, 5, FUNCTIONDIR."?busca=$busca&Page=", 'Primeira', 'Ant.', 'Prox.', 'Última');


	$this->loadView('viewLogs');
    }
}
