<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
require_once('controllerPrincipal.php');


class ReservaSalas extends controllerPrincipal{

    function __construct() {
		parent::__construct();
		$this->libs->Permission = 'GU_TI_SCSM_Perm_SuporteInfra';
    }

    public function index(){
    	$this->coreView['title']='Reserva de Salas de Reunião';

    	$this->loadModel('modelReservaSalas', 'model');
    	if(isset($_GET['reservar'])){
    		$temp = explode('_', $_GET['reservar']);
    		$reservar['sala'] = $temp[2];
    		$reservar['usuario'] = $temp[3];
    		$reservarData = explode('-', $temp[0]);
    		$reservarHora = explode(':', $temp[1]);
    		$reservar['horario'] = date('Y-m-d H:i:s', mktime($reservarHora[0],$reservarHora[1],0, $reservarData[1], $reservarData[0], $reservarData[2]));
    		$exec = $this->models->model->reservar($reservar);
			echo json_encode($exec); die();
    		//header('Location: '.CONTROLLERDIR.'?dia='.$temp[0]);
    	}

    	if(isset($_GET['reservarDia'])){
    		$temp = explode('_', $_GET['reservarDia']);

    		$horarios = $this->models->model->listarHorarios();

    		foreach($horarios as $horario){
				$reservar['horario'] = date('Y-m-d', strtotime(date('d-m-Y', strtotime($temp[0])))).' '.$horario['horario'].':00';
				$reservar['usuario'] = $temp[2];
				$reservar['sala'] = $temp[1];
				$exec = $this->models->model->reservarDia($reservar);
    		}
    		header('Location: '.CONTROLLERDIR.'?dia='.$temp[0]);
    	}

    	if(isset($_GET['liberar'])){
    		$temp = explode('_', $_GET['liberar']);
    		$reservar['sala'] = $temp[2];
    		$reservar['usuario'] = $temp[3];
    		$reservarData = explode('-', $temp[0]);
    		$reservarHora = explode(':', $temp[1]);
    		$reservar['horario'] = date('Y-m-d H:i:s', mktime($reservarHora[0],$reservarHora[1],0, $reservarData[1], $reservarData[0], $reservarData[2]));
    		$exec = $this->models->model->liberar($reservar);
			echo json_encode(1); die();
    		//header('Location: '.CONTROLLERDIR.'?dia='.$temp[0]);
    	}

    	if(isset($_GET['dia'])){
    		$data = $_GET['dia'];
    	}else{
    		$data = date('d-m-Y');
    	}

    	$dataExplode = explode('-', $data);
    	$dataExplode = date('Y-m-d', mktime(0,0,0, $dataExplode[1], $dataExplode[0], $dataExplode[2]));

		$reservas = $this->models->model->listarReservas($dataExplode);
		$salas = $this->models->model->listarSalas();
		$horarios = $this->models->model->listarHorarios();

    	$i = 0;
    	$dias = array();
    	for ($i = 0; $i<=22; $i++){
			$dataArray = $this->somar_dias_uteis(date('d/m/Y'), $i);
			//$dataGerada = date('d/m/Y', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]. " +$i days"));
			if($dataArray){
				$dataArray = explode('/', $dataArray);
    			$dia		= date('d', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]));
				$mes		= date('m', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]));
				$ano		= date('Y', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]));
				$semana	= date('w', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]));
				$ano = date('Y', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]));

    			//switch ($mes){ case 1: $mes = "Jan"; break; case 2: $mes = "Fev"; break; case 3: $mes = "Mar"; break; case 4: $mes = "Abr"; break; case 5: $mes = "Mai"; break; case 6: $mes = "Jun"; break; case 7: $mes = "Jul"; break; case 8: $mes = "Ago"; break; case 9: $mes = "Set"; break; case 10: $mes = "Out"; break; case 11: $mes = "Nov"; break; case 12: $mes = "Dez"; break;}
				switch ($semana){ case 0: $semana = "Dom"; break; case 1: $semana = "Seg"; break; case 2: $semana = "Ter"; break; case 3: $semana = "Qua"; break; case 4: $semana = "Qui"; break; case 5: $semana = "Sex"; break; case 6: $semana = "Sab"; break;}


				if($i == 0){
					$semana = '<b>(Hoje)</b> ';//.$semana;
				}

    			if(date('d-m-Y', strtotime("+$i days")) == $data){
    				array_push($dias, array('view' => $semana.' - '.$dia.'/'.$mes.'/'.$ano, 'link' => date('d-m-Y', strtotime("+$i days")), 'class' => 'active'));
    			}else{
    				array_push($dias, array('view' => $semana.' - '.$dia.'/'.$mes.'/'.$ano, 'link' => date('d-m-Y', strtotime("+$i days")), 'class' => ''));
    			}
			}
    	}

		//calcula calendario para aparesentar no campo de Outras Datas
		$dataMaxima = strtotime("+21 days");
		$dataCorrente =  explode('-', $data);
		$dataCorrente = mktime(0, 0, 0, $dataCorrente[1], $dataCorrente[0], $dataCorrente[2]);

		if($dataMaxima <= $dataCorrente){
			$dataCorrente = date('d/m/Y', $dataCorrente);
			$this->coreView['dataCorrente'] = $dataCorrente;
		}




    	$tabela = array();
    	foreach($horarios as $horario){
    		$horario = $horario['horario'];
    		$tabela[$horario] = array();
    			foreach($salas as $sala){
    				if($reservas){
    					foreach($reservas as $reserva){
	    					if(substr($reserva['horario'],11, strlen($reserva['horario'])-14) == $horario && $reserva['sala'] == $sala['id']){


	    						foreach($this->coreView['usuario']['memberof'] as $item) {
									if($item == 'CN=csc.processo,OU=Email,OU=Grupos,OU=Servicos,DC=maggi,DC=corp' || $item == 'CN=apoio.administrativo,OU=Email,OU=Grupos,OU=Servicos,DC=maggi,DC=corp')
									{
										if($reserva['usuario'] == $this->coreView['usuario']['samaccountname'][0]){
											$cor = '#11AA11';
										}else{
											$cor = '#888';
										}
										$tabela[$horario][$sala['nome']] = '<i style="color: '.$cor.'">'.$reserva['usuario'].'</i>';
										goto autorizado;
									}
    								else
 									{
										 if($reserva['usuario'] == $this->coreView['usuario']['samaccountname'][0]){
											 $cor = '#11AA11';
										 }else{
											 $cor = '#888';
										 }

 										if($reserva['usuario'] == $this->coreView['usuario']['samaccountname'][0]){
											 $tabela[$horario][$sala['nome']] = '<i style="color: '.$cor.'">'.$reserva['usuario'].'</i>';
			    						}else{
			    							//foreach($this->coreView['usuario']['memberof'] as $item) {
												//if($item == 'CN=apoio.administrativo,OU=Email,OU=Grupos,OU=Servicos,DC=maggi,DC=corp' || $item == 'CN=csc.processo,OU=Email,OU=Grupos,OU=Servicos,DC=maggi,DC=corp')
			    								//	$tabela[$horario][$sala['nome']] = '<i style="color: #CCCCCC">Reservado1aaa</i>';
			    								//else
			    									$tabela[$horario][$sala['nome']] = '<i style="color: #CCCCCC">'.$reserva['usuario'].'</i>';
			    							//}

			    							///VER SE FUNCIONA
			    							if(isset($usuario['memberof'])){
				    							foreach($this->coreView['usuario']['memberof'] as $item) {
													if(strpos($item, $this->libs->Permission)){
														$tabela[$horario][$sala['nome']] = '<i style="color: #AA1111">'.$reserva['usuario'].'</i>';
													}
												}
											}
											///VER SE FUNCIONA



			    						}
 									}

								}
								autorizado:




	    						BREAK;
	    					}else{
	    						$tabela[$horario][$sala['nome']] = 'Sala Livre';
	    					}
	    				}
    				}else{
    					$tabela[$horario][$sala['nome']] = 'Sala Livre';
    				}

    		}
    	}

    	$this->coreView['salas'] = $salas;
    	$this->coreView['horarios'] = $horarios;
    	$this->coreView['dias'] = $dias;
    	$this->coreView['data'] = $data;
    	$this->coreView['tabela'] = $tabela;
    	$this->coreView['reservas'] = $reservas;

		//var_dump($reservas); die();

		$this->loadView('viewReserva');
    }

	private function somar_dias_uteis($data,$dias,$resursivo=0) {
		$dataArray = explode('/', $data);
		$dataGerada = date('d/m/Y', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]. " +$dias days"));
		$fdsDataGerada = date('w', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]. " +$dias days"));
		//$fdsData = date('w', strtotime($dataArray[2].'-'.$dataArray[1].'-'.$dataArray[0]));


		if($fdsDataGerada == 0 || $fdsDataGerada == 6){
			return null;
			//$resursivo++;
			//$dataGerada = $this->somar_dias_uteis($data, $dias+($resursivo*2), $resursivo);
		}



		return $dataGerada;
	}
}
