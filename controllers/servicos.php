<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
require_once('controllerPrincipal.php');

class Servicos extends controllerPrincipal{
	
	
	
	
	function __construct() {
		parent::__construct();
		//Permissoes
		$this->loadLib('Permission');
		$this->libs->Permission->required = array('GU_TI_SCSM_Perm_SuporteInfra','csc.processo', 'GU_CRP_ACL_GestaoTI');
		$this->libs->Permission->verify($this->coreView['usuario']['memberof']);
		//Permissoes
		
		
		$this->loadModel('modelLink', 'modelLink');
	}


	public function index()
	{
		$this->coreView['title']='Monitoramento de Serviços';
		
		$this->coreView['listaLinks'] = $this->models->modelLink->getServicos();
		//$this->coreView['geral'] = $this->models->modelLink->getGeral();
		$now = new DateTime(Date('Y-m-d H:i:s'));
		$date = new DateTime($this->models->modelLink->getTempo());
		$tempo = $now->getTimestamp() - $date->getTimestamp();
		$ultimaVerificacao = 'Última verificação há '.floor($tempo/60).'m atrás.';
		if($tempo < 120){
			if($tempo>10)
				$ultimaVerificacao = 'Última verificação há <b>'.$tempo.'s</b> atrás, às <b>'.$date->format('H:i d/m/Y').'</b>';
			else
				$ultimaVerificacao = 'Última verificação <b>exatamente agora</b>, às <b>'.$date->format('H:i d/m/Y').'</b>';
		}else{
			$ultimaVerificacao = 'Última verificação há '.floor($tempo/60).'m'.' atrás.';
			if($tempo > 300){
				$this->coreView['title']='<b style="color: red">MONITOR DE SERVIÇOS DESATIVADO.</b><br><small>Favor subir o serviço "ServicoMonitor" do SRVVM190</small>';
			}
		}

		$this->coreView['ultimaVerificacao'] = $ultimaVerificacao;
		
		$this->loadView('viewServicos');
		echo "<meta HTTP-EQUIV='refresh' CONTENT='30;URL=''>";
    }
    
    public function monitoramento(){
    	$this->coreView['title']='Dashboard';
    	
    	
    	$this->loadModel('modelLog', 'modelLog');
    	$this->loadModel('modelLink', 'modelLink');
    	
    	//usuarios
		$relatorioUsuarios = array();
		$relatorioUsuarios['Auto desbloqueios'] = $this->models->modelLog->count('Conta desbloqueada.');
		$relatorioUsuarios['Desbloqueios pela equipe'] = $this->models->modelLog->count('DESBLOQUEOU');
		$relatorioUsuarios['Reset de senha pela equipe'] = $this->models->modelLog->count('RESETOU ');
		$this->coreView['relatorioUsuarios'] = $relatorioUsuarios;
		
		//links
		$relatorioLinks = array();
		$links = $this->models->modelLink->getLinks();
		foreach($links as $link){
			$relatorioLinks[$link['titulo']] = $this->models->modelLog->count($link['titulo'].' está operando normalmente');
		}
		arsort($relatorioLinks);
		//$this->coreView['relatorioLinks'] = $relatorioLinks;
		
		//links offline
		$relatorioOffline = array();
		foreach($links as $link){
			if($link['statusLink'] == 0){
				$relatorioOffline[$link['titulo']] = '';
			}
		}
		//$this->coreView['relatorioOffline'] = $relatorioOffline;
		
		//VPNs offline
		$relatorioVPN = array();
		foreach($links as $link){
			if($link['statusVPN'] == 1){
				$relatorioVPN[$link['titulo']] = '';
			}
		}
		//$this->coreView['relatorioVPN'] = $relatorioVPN;
		
		//tempos
		
		$time = time() - $this->models->modelLink->getTempo();
		$tempo = $time.'s';
		if($tempo < 120){
			if($tempo>10)
				$ultimaVerificacao = 'Última verificação há <b>'.$tempo.'s</b> atrás, às <b>'.$date->format('H:i d/m/Y').'</b>';
			else
				$ultimaVerificacao = 'Última verificação <b>exatamente agora</b>, às <b>'.$date->format('H:i d/m/Y').'</b>';
		}else{
			$ultimaVerificacao = 'Última verificação há '.floor($tempo/60).'m'.' atrás.';
			if($tempo > 300){
				$this->coreView['title']='<b style="color: red">MONITOR DE SERVIÇOS DESATIVADO.</b><br><small>Favor subir o serviço "ServicoMonitor" do SRVVM190</small>';
			}
		}
		
		
		$this->loadView('viewDashboard', get_defined_vars());
    }
    
    public function alterar(){
		$this->coreView['title']='Alteração do Cadastro do Link';
		$this->coreView['geral'] = $this->models->modelLink->getGeral();

		
		if(isset($_GET)){
			$link = $this->models->modelLink->getLink($_GET['id']);
			$cmd_ativaVPN = $link['cmd_ativaVPN'];
			$cmd_ativaVPN = str_replace('|',"\n", $cmd_ativaVPN);
			$link['cmd_ativaVPN'] = $cmd_ativaVPN;
			
			$cmd_desativaVPN = $link['cmd_ativaVPN'];
			$cmd_desativaVPN = str_replace('|',"\n", $cmd_desativaVPN);
			$link['cmd_desativaVPN'] = $cmd_desativaVPN;
			
			$this->coreView['link'] = $link;
		}
		
		if($_POST){
				$link = $_POST;
				$cmd_ativaVPN = $link['cmd_ativaVPN'];
				$cmd_ativaVPN = explode("\n ", $cmd_ativaVPN);
				$cmd_ativaVPN = implode("\n", $cmd_ativaVPN);
				$cmd_ativaVPN = explode("\n", $cmd_ativaVPN);
				var_dump($cmd_ativaVPN);
				$this->models->modelLink->update($_POST);
				header('Location: gerenciar');
		}
		
		$this->loadView('viewLinksAdicionar');
		}
		
		public function deletar(){
		$this->coreView['title']='Deletar Cadastro do Link';

		
		if(isset($_GET)){
			$link = $this->models->modelLink->getLink($_GET['id']);
			$this->models->modelLink->delete($link);
		}
		
		
		header('Location: gerenciar');
    }
    
    public function gerenciar(){
    	
    	//Permissoes
		$this->loadLib('Permission');
		$this->libs->Permission->required = array('GU_TI_SCSM_Perm_SuporteInfra');
		$this->libs->Permission->verify($this->coreView['usuario']['memberof']);
		//Permissoes
		
		
		$this->coreView['title']='Lista de Serviços';
		$this->loadLib('make');
		$this->coreView['geral'] = $this->models->modelLink->getGeral();
		
		$listaNova = array();
		$lista = $this->models->modelLink->getServicos();
		foreach($lista as $link){
			unset($link['statusLink']);
			unset($link['tipo_servico']);
			unset($link['statusVPN']);
			unset($link['mensagem']);
			unset($link['aguardando']);
			unset($link['ip_alvo']);
			unset($link['cmd_ativaVPN']);
			unset($link['cmd_desativaVPN']);
			unset($link['vpnAuto']);
			$link['edicao'] = $this->libs->make->link('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', MODULEDIR.'/servicos/Alterar?id='.$link['id'], 'btn btn-success');
			$link['delete'] = $this->libs->make->link('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', MODULEDIR.'/servicos/Deletar?id='.$link['id'], 'btn btn-danger');
			array_push($listaNova, $link);
		}

		
		$this->coreView['listaLinks'] = $listaNova;
		$this->coreView['table'] = $this->libs->make->table(array('#', 'Localidade', 'Ip do Serviço', 'Porta', 'Timeout', 'Editar', 'Deletar'), $this->coreView['listaLinks'], 'table table-bordered');
		
		$this->loadView('viewLinkslistar');
		}
		
		public function adicionar(){
		$this->coreView['title']='Adicionar Link';

		$this->coreView['listaLinks'] = $this->models->modelLink->getLinks();
		$this->coreView['geral'] = $this->models->modelLink->getGeral();
		
		if($_POST){
			$array = $_POST;
			$array['tipo_servico'] = 1;
			$this->models->modelLink->salvarLink($array);
			header('Location: gerenciar');
		}
		
		$this->loadView('viewLinksAdicionar');
	}
	
	public function Logs(){
		
		$this->loadModel('modelLink', 'modelLog');
		$this->loadLib('Pagination');
		$this->loadLib('make');
		$this->coreView['title']='Logs de Serviços';
		
		$divisao = 100;
		
		$page = 1;
		if(isset($_GET['Page'])){
			$page = $_GET['Page'];
		}
		
		$busca= '';
		if(isset($_GET['busca'])){
			$busca = $_GET['busca'];
		}
		
		$logs = $this->models->modelLog->getLogsServico($page, $divisao, $busca);
		$total = $this->models->modelLog->countLogsServico($busca);
		$novoLogs = array();
		if(is_array($logs)){
			foreach($logs as $log){
				unset($log['id']);
				array_push($novoLogs, $log);
			}
		}else{
			$this->coreView['msgLog']= 'Sua busca não retornou resultados';
			$this->coreView['msgLogType']= 'info';
		}
		
		
		$this->coreView['logs']= $this->libs->make->table(array('Horário', 'Localidade', 'Timeout Min (ms)', 'Timeout Mid', 'Timeout Max', 'Ação'), $novoLogs, 'table table-striped');
		$this->coreView['pagination'] = $this->libs->Pagination->render($total, $page, $divisao, 10, FUNCTIONDIR."?busca=$busca&Page=", 'Primeira', 'Ant.', 'Prox.', 'Última');
		
		
		$this->loadView('viewLogs');
    }
    
 
}

