<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
require_once('controllerPrincipal.php');

class Servidor extends ModularCore\Controller{

	function __construct() {
		parent::__construct();
		$this->loadModel('modelLink', 'modelLink');
	}


	public function index(){
		$this->coreView['title']='Monitoramento de link e roteamento';
		
		$this->coreView['listaLinks'] = $this->models->modelLink->getLinks();
		$this->coreView['geral'] = $this->models->modelLink->getGeral();
		$this->coreView['usuario'] = array('displayname' => array('INTERFACE DE VERIFICAÇÃO'));
		$this->coreView['ultimaVerificacao'] = 'INTERFACE DE VERIFICAÇÃO';
		
		//if(isset($_GET['admin'])){
			$this->coreView['admin'] = '';
		//}
		
		$this->models->modelLink->gravaTempo();
		
		$this->loadLib('session');
		$this->loadModel('modelLog');
		if($this->libs->session->getData('ultimaVerificacao')){
			$cookie = $this->libs->session->getData('ultimaVerificacao');
			$target = date('Y/m/d H:i', strtotime("$cookie + 10 minutes"));
			if($target <= date('Y/m/d H:i')){
				$this->libs->session->setData('ultimaVerificacao', date('Y/m/d H:i'));
				$return = $this->models->modelLog->write('Monitoramento automático ativo', 'SISTEMA DE LINKS');
			}
		}else{
			$this->libs->session->setData('ultimaVerificacao', date('Y/m/d H:i'));
			$return = $this->models->modelLog->write('Monitoramento automático ativo', 'SISTEMA DE LINKS');
		}
		
		//$return = $this->models->modelLog->write('Verificando links', 'SISTEMA DE LINKS');
		
		
		
		$this->loadView('viewLinksComutacao');
		echo "<meta HTTP-EQUIV='refresh' CONTENT='120;URL=''>";
	}

}