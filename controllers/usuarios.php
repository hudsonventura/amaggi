<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');
require_once('controllerPrincipal.php');


class Usuarios extends controllerPrincipal {
	function __construct() {
		parent::__construct();
		$this->loadModel('modelLog', 'modelLog');
    }

	public function index()
	{

			$this->loadLib('Permission');
			$this->libs->Permission->required = array('GG_TI_Operadores_de_Conta', 'csc.processo', 'GU_CRP_ACL_GestaoTI', 'GG_TI_AccountOperators', 'CN=GU_TI_SCSM_Perm_SuporteInfra');
			$permissao = $this->libs->Permission->verify($this->coreView['usuario']['memberof']);
			if(!$permissao){
				die("Voce nao tem permissao para acessar essa ferramenta");
			}
			//Permissoes


			$this->coreView['title'] = 'Manutenção de usuários';
			$this->coreView['msg'] = $this->libs->session->getTemp('msg');
			$this->coreView['msgType'] = $this->libs->session->getTemp('msgType');

			$domain = $_GET['domain'];

			$this->coreView['listaUsuarios'] = array();
			if(isset($_GET['buscaUsuario']) && ($_GET['field'] == 'samaccountname' || $_GET['field'] == 'description')){
				if($domain == 'all'){
					if($_GET['buscaUsuario'] <> ''){
						$listaUsuarios = $this->libs->activedirectory->searchUser($_GET['field'].'=*'.$_GET['buscaUsuario'].'*');
					}else{
						$listaUsuarios = $this->libs->activedirectory->searchUser();
					}

				}else{
					if($_GET['buscaUsuario'] == ''){
						$listaUsuarios = $this->libs->activedirectory->searchDomainUserBlocked($_GET['field'].'=*'.$_GET['buscaUsuario'].'*', $domain);
					}else{
						$listaUsuarios = $this->libs->activedirectory->searchDomainUser($_GET['field'].'=*'.$_GET['buscaUsuario'].'*', $domain);
					}
					if(($_GET['field'] == 'group') && $_GET['buscaUsuario'] <> ''){
						echo 'teste';
					}
				}
			}else{
				if(isset($_GET['field']) && ($_GET['field'] == 'samaccountname' || $_GET['field'] == 'description')){
					header('Location: '.MODULEDIR.'/usuarios?buscaUsuario=&field=samaccountname&domain=all');
				}else{
					if(isset($_GET['buscaUsuario']) && $_GET['field'] == 'group' ){
					//$this->coreView['listaGrupos'] = $this->libs->activedirectory->searchGroups($_GET['buscaUsuario'], $_GET['domain']);
					}
				}
			}
			$ordenar = [];
			foreach ($listaUsuarios as $key => $usuario) {
				foreach ($usuario as $key => $user) {
					array_push($ordenar, $user);
				}
				
			}
			usort($ordenar, 'sortByOrder');
			$this->coreView['listaUsuarios'] = array($ordenar);

		$this->loadView('viewUsuarios');
		echo "<meta HTTP-EQUIV='refresh' CONTENT='60;URL=''>";
	}

	public function resetSenha($usuarioAD = null){
		
		
		
		$usuario = array('samaccountname'=>array(), 'domain'=>'');
		if(isset($_GET['samaccountname'])){
			$usuario['samaccountname'] = $_GET['samaccountname'];
			$usuario['domain'] = 'all';
		}
		
		if(isset($usuarioAD)){
			$usuario['samaccountname'] = $usuarioAD['samaccountname'];
		}


		
		if($usuario['samaccountname']){
		    $users = $this->libs->activedirectory->getUser('samaccountname='.$usuario['samaccountname'], 'all');
		    if(!$users){
				$this->libs->session->setTemp('msg', "Usuário <b>".$usuario['samaccountname']."</b> não encontrado no AD ");
				$this->libs->session->setTemp('msgType', 'danger');
				echo "<script type='text/javascript'>
						
							window.setTimeout(function(){history.go(-1)},100);
						</script>";
				return false;
			}

		    //$reset = $this->libs->activedirectory->changePass($user, $newPass); //TODO: Reativar reset de senha do AD pelo IIS
			
			/*if(!array_search('nusoap_client', get_declared_classes())){
				$this->loadLib('composer');
				$this->libs->composer->autoload();
			}*/

		    $newPass = 'Amaggi@'.date('His');
			foreach ($users as $key => $user) {
				$object = array();
				$object['userDN'] = $user['dn'];
				$object['userPassword'] = $newPass;

				$domain = $user['domain'];


				//informa que quando usuario é da amaggi e empresas do grupo, a senha PRECISA SER ALTERADA NO PRIMEIRO ACESSO.
				//caso o usuario nao seja da amaggi, ou seja, consultores, NÃO DEVE CUMPRIR COM A EXIGENCIA DE ALTERAÇÃO DE SENHA.
				$mail = (isset($user['mail'][0])) ? explode('@', $user['mail'][0])[1] : null ;
				if($mail && ($mail == 'amaggisf.com.br' || $mail == 'unitapajos.com.br' || $mail == 'alzgraos.com.br' || $mail == 'fundacaoalm.org.br')){
					$mail = explode('@', $user['mail'][0])[1];
					$resetJV =$this->libs->activedirectory->resetPasswordAD($object['userDN'], $newPass, $domain, false);
					if(!$resetJV){
						var_dump($this->libs->activedirectory->lastError);
					}
				}else{
					//$client = new nusoap_client(Modularcore\core::$coreConfig['webservice_soap_Personal']);
					//$response1 = $client->call('resetPasswordAD', $object);
					$resetMaggi = $this->libs->activedirectory->resetPasswordAD($object['userDN'], $newPass, $domain, false);
					if(!$resetMaggi){
						var_dump($this->libs->activedirectory->lastError);
					}
				}

				
				
				if(isset($resetMaggi) || isset($resetJV)){
					$response2 = $this->libs->activedirectory->unblockUser($user, $domain); //forçar trocar a senha com --isset($resetMaggi)
					$this->libs->session->setTemp('msg', "Nova senha <b>temporária</b> para o usuario ".$user['samaccountname'][0].": <b>$newPass</b><br><b>Será necessário a troca da senha pelo usuario no logon</b>");
					$this->libs->session->setTemp('msgType', 'success');
					$this->models->modelLog->write('RESETOU a senha de '.$user['samaccountname'][0], $this->usuario['samaccountname'][0]);
				}else{
					$this->libs->session->setTemp('msg', "Erro ao tentar resetar a senha do usuario");
					$this->libs->session->setTemp('msgType', 'danger');
					echo "<script type='text/javascript'>
							
								window.setTimeout(function(){history.go(-1)},100);
							</script>";
					return $newPass;
				}
			}



		    
			

					

					/*envio de email de confirmação de reset de senha
					$this->loadLib('Mail');
					$this->libs->Mail->AddAddress('hudson.ventura@amaggi.com.br', 'Centro de Serviço Compartilhado');
					//$this->libs->Mail->AddAddress('suporte.infra@amaggi.com.br', 'Centro de Serviço Compartilhado');
					$this->libs->Mail->AddAddress($user['mail'][0], 'Centro de Serviço Compartilhado');

					$this->libs->Mail->From = 'suporte.infra@amaggi.com.br';
					$this->libs->Mail->FromName = 'Sistema de Desbloqueio e Reset de Senha de Usuários';


					$this->libs->Mail->Subject  = "Reset de Senha pelo Gestor";
					$this->libs->Mail->Body =
											'<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
												<html>
												  <head>
												    <meta http-equiv="Content-Type" content="text/html;UTF-8" />
												  </head>
												  <body style="margin: 0px; background-color: #F4F3F4; font-family: Helvetica, Arial, sans-serif; font-size:12px;" text="#444444" bgcolor="#F4F3F4" link="#21759B" alink="#21759B" vlink="#21759B" marginheight="0" topmargin="0" marginwidth="0" leftmargin="0">
												    <table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F4F3F4">
												      <tbody>
												        <tr>
												          <td style="padding: 15px;"><center>
												            <table width="100%" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
												              <tbody>
												                <tr>
												                  <td align="left">
												                    <div style="border: solid 1px #d9d9d9;">
												                      <table id="header" style="line-height: 1.6; font-size: 12px; font-family: Helvetica, Arial, sans-serif; border: solid 1px #FFFFFF; color: #444;" border="0" width="93%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
												                        <tbody>
												                          <tr>
												                            <td style="color: #ffffff;" colspan="2" valign="bottom" height="30">.</td>
												                          </tr>
												                          <tr>
												                            <td style="line-height: 32px; padding-left: 30px;" valign="baseline"><span style="font-size: 32px;"><a style="text-decoration: none;" href="https://suporte.amaggi.com.br" target="_blank"><img class="alignnone size-full wp-image-21" src="https://suporte.amaggi.com.br/repositorios/imagens/csc/logo_csc.png" alt="Logo-CSC" /></a></span></td>
												                            <td style="padding-right: 30px;" align="right" valign="baseline"><span style="font-size: 14px; color: #777777;">Sistema de Desbloqueio e Reset de Senha de Usuários</span></td>
												                          </tr>
												                        </tbody>
												                      </table>
												                      <table id="content" style="margin-top: 15px; padding-left: 30px; color: #444; line-height: 1.6; font-size: 12px; font-family: Arial, sans-serif;" border="0" width="93%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
												                        <tbody>
																			<tr>
																				<td colspan="2">
																					<div style="padding: 15px; border-top: solid 1px #d9d9d9;"><br /><br />
																						A senha do usuário <b>'.$user['samaccountname'][0].'</b> foi resetada por <b>'.$this->usuario['samaccountname'][0].'</b><br /><br />


																						<br />
																						<div style="border-top: solid 1px #d9d9d9;width: 100%;font-size: 11px;color: #999999;font-family: arial,sans-serif;"><br />
																							Email enviado de forma automática</p>
																							<p>
																								Para acessar o sistema e verificar o estado atual, visite: <a href="https://suporte.amaggi.com.br" rel="nofollow">https://suporte.amaggi.com.br</a><br />
																							</p>
																						</div>
																						<br />
																					</div>
																				</td>
																			</tr>
												                        </tbody>
												                      </table>

												                    </div>
												                  </td>
												                </tr>
												              </tbody>
												            </table>
												            </center>
															</td>
												        </tr>
												      </tbody>
												    </table>
												  </body>
												</html>';

					$resultadoEmail = $this->libs->Mail->Send();
					echo "<script type='text/javascript'>
							
								window.setTimeout(function(){history.go(-1)},100);
							</script>";
					return $newPass;*/
				


		}
		//header('Location: '.MODULEDIR.'/usuarios/acessoGestor');
		echo "<script type='text/javascript'>
				
					window.setTimeout(function(){history.go(-1)},100);
				</script>";
    }

    public function desbloquearUsuario(){
		$this->coreView['title'] = 'Desbloqueio de usuário';

		if(isset($_GET['domain'])){
			$domain = $_GET['domain'];
		}else{
			$domain = 'all';
		}



		if(isset($_GET['samaccountname'])){
			$user = $this->libs->activedirectory->getUser('samaccountname='.$_GET['samaccountname'], $domain);
			
			if($domain <> 'all'){
				$user = ['unicoDominio' => $user];
			}
			$msgRetorno = '';
			foreach ($user as $key => $item) {
				try{
					$return = $this->libs->activedirectory->unblockUser($item, $item['domain']);
	
					$validar = true;
				}catch(Exception $e){
					$return = $e->getMessage();
					$validar = false;
				}
				//var_dump($return);
				if(isset($return)){
					$return = 'Sem permissão para executar a operação.';
				}
	
				if($validar){
					$msgRetorno .= "Usuário <b>".$item['samaccountname'][0]."</b> do dominio <b>".$item['domain']."</b> desbloqueado com sucesso!<br>";
				}else{
					$mssgRetorno .= "Não foi possível desbloquear o usuário <b>".$item['samaccountname'][0]."</b> do dominio <b>".$item['domain']."</b>. <br>";
				}
			}
		}

		$this->libs->session->setTemp('msgType', 'success');
		$this->libs->session->setTemp('msg', $msgRetorno);
		$this->models->modelLog->write('DESBLOQUEOU a conta de '.$item['samaccountname'][0], $this->usuario['samaccountname'][0]);



		if(isset($_GET['page'])){
			header('Location: '.MODULEDIR.'/'.$_GET['page']);
		}else{
			header('Location: '.MODULEDIR.'/usuarios?buscaUsuario=&field=samaccountname&domain=all');
		}


	    }

    public function analisar(){
		var_dump($this->libs->activedirectory->getUser('samaccountname='.$_GET['usuario']));
    }


	public function definirSenha($object = null){
		

		if(isset($_GET['samaccountname']) && isset($_GET['userPassword'])){
		    $users = $this->libs->activedirectory->getUser('samaccountname='.$_GET['samaccountname']);
		    $newPass = $_GET['userPassword'];

			//$reset = $this->libs->activedirectory->changePass($user, $newPass); //TODO: Reativar reset de senha do AD pelo IIS
			
			//var_dump($reset); die();

			$object = array();
			$object['userPassword'] = $newPass;

			$this->loadLib('composer');
			$this->libs->composer->autoload();

			foreach ($users as $key => $user) {
				$object['userDN'] = $user['dn'];
				$domain = $user['domain'];

				$response1 = $this->libs->activedirectory->resetPasswordAD($object['userDN'], $newPass, $domain, false);
				$response2 = $this->libs->activedirectory->unblockUser($user, $user['domain']);

				if($response1 && $response2){
					$this->libs->session->setTemp('msg', "Nova senha <b>temporária</b> para o usuario ".$user['samaccountname'][0].": <b>$newPass</b><br><b>Será necessário a troca da senha pelo usuario no logon</b>");
					$this->libs->session->setTemp('msgType', 'success');
					$this->models->modelLog->write('RESETOU a senha de '.$user['samaccountname'][0], $this->usuario['samaccountname'][0]);
				}else{
					$this->libs->session->setTemp('msg', "Erro ao tentar resetar a senha do usuario:");
					$this->libs->session->setTemp('msgType', 'danger');
				}
				

			}
		}else{
			$this->libs->session->setTemp('msg', "Voce nao possui acesso a este recurso");
					$this->libs->session->setTemp('msgType', 'danger');
		}

		header('Location: '.MODULEDIR.'/usuarios?buscaUsuario='.$_GET['samaccountname'].'&field=samaccountname&domain=all');
		//
	}

	public function habilitarLogon(){//$samaccountname, $domain, $turn){

		if(isset($_GET['samaccountname']) && isset($_GET['domain']) && isset($_GET['turn'])){
			$samaccountname = $_GET['samaccountname'];
			$domain = $_GET['domain'];


		}
		else {die('<h2>You cannot access this. Go back or die in my hands</h2>');}



			$haveGroup = $this->libs->activedirectory->listUsersofaGroup('memberOf=CN=GU_Lock_User_Excecao,OU=Recursos,OU=Grupos,OU=Servicos,DC=maggi,DC=corp');
			$notHaveGroup = $this->libs->activedirectory->listUsersofaGroup('memberOf=CN=GU_CRP_ACL_CSC,OU=Compartilhamento,OU=Grupos,OU=Servicos,DC=maggi,DC=corp) (!(memberOf=CN=GU_Lock_User_Excecao,OU=Recursos,OU=Grupos,OU=Servicos,DC=maggi,DC=corp)');



			$jsonResult = array();
			foreach($haveGroup as $have){
				//var_dump($have['samaccountname'][0]);
				$result = $this->libs->activedirectory->changeLogonHours($have, 'ffffffffffffffffffffffffffffffffffffffffff');
				if(is_array($result)){
					array_push($jsonResult, $result);
				}

			}

			foreach($notHaveGroup as $notHave){
				$result = $this->libs->activedirectory->changeLogonHours($notHave, '00000000fc3f00fc3f00fc3f00fc3f00fc3f000000');
				if(is_array($result)){
					array_push($jsonResult, $result);
				}
			}

			echo json_encode($jsonResult);



	}

	function utf8_converter($array)
	{
	    array_walk_recursive($array, function(&$item, $key){
	        if(!mb_detect_encoding($item, 'utf-8', true)){
	                $item = utf8_encode($item);
	        }
	    });

	    return $array;
	}


	//NAO FUNCIONA =/
	public function webservice(){
		$this->loadLib('nusoap/nusoap');
		$this->libs->soapServer = new ModularCore\soap_server();
		$this->libs->soapServer->configureWSDL('WebService de Usuarios no ModularCore','http://desenv/ModularCore/amaggi/usuarios/webservice?wsdl','http://desenv/ModularCore/amaggi/usuarios/webservice?wsdl');
		$this->libs->soapServer->wsdl->schemaTargetNamespace = 'http://desenv/ModularCore/amaggi/usuarios/webservice?wsdl';
		$this->libs->soapServer->schemaTargetNamespace = 'http://desenv/ModularCore/amaggi/usuarios/webservice?wsdl';
		$this->libs->soapServer->SOAPAction = 'http://desenv/ModularCore/amaggi/usuarios/webservice';
		$this->libs->soapServer->register('habilitarLogon', 																														//nome do método
								array('samaccountname' => 'xsd:string','domain' => 'xsd:string','turn' => 'xsd:string'),							//parâmetros de entrada
								array('sucesso' =>'xsd:string','mensagem' =>'xsd:string'),																	//parâmetros de saída
								'hudsonventura@gmail.com',																												//documentação do serviço
								'http://desenv/ModularCore/amaggi/usuarios/webservice?directLogin=hudson.ventura&directPass=%23Timetecnica001'
		);
		$data = @file_get_contents('php://input');
		$data = @isset($data) ? $data : '';
		@$this->libs->soapServer->soap_defencoding = 'utf-8';
		//var_dump($this->libs->soapServer);die();
		@$this->libs->soapServer->service($data);
	}


	public function acessoGestor(){
		
		//Permissoes
		$this->loadLib('Permission');
		$this->libs->Permission->required = array('supervisor','gerente','diretor','presidente','acionista','especialista','controller', 'lider', 'coord', 'engenheiro', 'projetos');

		$cargo = $this->coreView['usuario']['HCM'][0]['CARGO'];

		$permissaoAcesso = $this->libs->Permission->verify(strtolower($cargo));
		if(!$permissaoAcesso){
			die("<h1>ACESSO NEGADO</h1><br><br><h3>Somente supervisores, gerentes, diretores, especialistas, controllers, lideres, coordenadores e engenheiros podem acessar essa pagina</h3><br>Seu cargo atual no RH é <b>$cargo</b>");
		}
		//Permissoes


		$this->loadModel('modelUsuario');
		$usuarios = $this->models->modelUsuario->getSubordinados($this->coreView['usuario']);


		$this->coreView['title'] = 'Gestão de usuários de rede';
		$this->coreView['msg'] = $this->libs->session->getTemp('msg');
		$this->coreView['msgType'] = $this->libs->session->getTemp('msgType');



		$listaCPFs = array();
		
		foreach ($usuarios as $key => $usuario) {
			array_push($listaCPFs, $usuario['CPF']);
		}
		$listaUsuarios = $this->libs->activedirectory->searchUsers("description", $listaCPFs);

		
		$varrer = $listaUsuarios[0];
		usort($varrer, 'sortByOrder');
		$this->coreView['listaUsuarios'] = array($varrer);


		$this->loadView('viewUsuarios');
		echo "<meta HTTP-EQUIV='refresh' CONTENT='60;URL=''>";

	}

	public function horaExtra(){

		if(isset($_REQUEST['samaccountname']) && isset($_REQUEST['description'])){
			$usuario = $_REQUEST['samaccountname'];
			$matricula = $_REQUEST['description'];
			$this->loadModel('modelUsuario');
			$resultado = $this->models->modelUsuario->abrirSolicitacaoExecucaoHoraExtra($usuario, $matricula, $this->usuario);

			if (is_array($resultado))
			{
				$this->libs->session->setTemp('msg', "Usuario <b>$usuario</b> liberado para execução de horas extras até as 07:00 de ".date('d/m/Y', strtotime("+1 day"))."<br><br>".$resultado['RecordID']);
				$this->libs->session->setTemp('msgType', 'success');
			}else{
				$this->libs->session->setTemp('msg', $resultado);
				$this->libs->session->setTemp('msgType', 'danger');
			}
			header('Location: '.MODULEDIR.'/usuarios/acessoGestor');

		}
		die();
	}

	//RESET DE SENHAS DE USUARIOS DO AD
	private function resetPasswordAD($userDN, $userPassword, $domain, $mustChangePass = true){
		$activeDirectory = $GLOBALS['coreConfig']['ActiveDirectory'][$domain];

		$return = array();
		try{
			$ADSI = new COM("LDAP:");
			$dnConnect = "LDAP://".$activeDirectory['host']."/".$userDN;
			$dsObject = $ADSI->OpenDSObject($dnConnect, $activeDirectory['admin_user'], $activeDirectory['admin_pass'], 1);
			$dsObject->AccountDisabled = false;
			$dsObject->SetPassword($userPassword);
			$dsObject->SetInfo();

			return true;
		}catch (Exception $e){
			var_dump($dnConnect, $e);
			$this->lastError = $e->getMessage();
			return false;
		}

		return $return;
	}
	
	
}

function sortByOrder($a, $b) {
    return ($a['samaccountname'][0] < $b['samaccountname'][0]) ? -1 : 1;
}