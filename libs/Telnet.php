<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');



class Telnet{

	public function sendCommands($host, $port, $commands){
	    $return = '';
	    
	    //return 'EXECUTOU!!!!';
	    
	    error_log('Accessing '.$host.' by the port '.$port, 0);
		$fp = fsockopen($host, $port, $errno, $errstr, 30);
		if (!$fp) {
			error_log('Fail to access '.$host, 0);
			$return = "$errstr ($errno)<br />\n";
		} else {
			error_log('Connected!', 0);
			$out = '';
			foreach($commands as $command){
				$out .= $command."\r\n";
			}
			//error_log($out, 0);
			fwrite($fp, $out);
			
			$count = 0;
			while (!feof($fp)) {
				$lineLog = fgets($fp, 2048);
				$line = $lineLog."<br />";
				error_log($lineLog, 0);
				$return =  $return.$line;
				if($line == "telnet@SWCORE1#1\r\n<br />" ||
					substr_count($line, "Invalid input")>0 ||
					substr_count($line, "Control-c")>0 ||
					substr_count($line, "Type ? for a list")>0
				){
					return $return;
				}
			}
			$return =  $return."END";
		}
		
		return $return;
	}
	
}