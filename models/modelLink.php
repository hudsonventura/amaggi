<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');


class modelLink extends ModularCore\Model{
	function __construct() {
		parent::__construct();
		$this->prefixtablename = '';
		$this->loadDb('links', 'links');
	}

	public function getLinks(){
		$this->db->links->sqlBuilder->select('*');
		$this->db->links->sqlBuilder->from('links');
		$this->db->links->sqlBuilder->where('tipo_servico = 0');
		$this->db->links->sqlBuilder->orderby('titulo');
		return $this->db->links->sqlBuilder->executeQuery();
	}
	
	public function getServicos(){
		$this->db->links->sqlBuilder->select('*');
		$this->db->links->sqlBuilder->from('links');
		$this->db->links->sqlBuilder->where('tipo_servico = 1');
		$this->db->links->sqlBuilder->orderby('titulo');
		return $this->db->links->sqlBuilder->executeQuery();
	}

    public function getGeral(){
		$this->db->links->sqlBuilder->select('*');
		$this->db->links->sqlBuilder->from('vpnAutoGeral');
		$return = $this->db->links->sqlBuilder->executeQuery();
		return $return[0];
    }
    
    public function setVpnAuto($campo, $valor){
		if($valor == 'true'){
		    $link['vpnAuto'] = 1;
		    $link['mensagem'] = 'VPN em modo AUTOMATICO';
		    $this->db->links->sqlBuilder->update($link, 'links');
		    $this->db->links->sqlBuilder->where('"id" = '.$campo);
		    return $this->db->links->sqlBuilder->executeQuery();
		}else{
		    $link['vpnAuto'] = 0;
		    $link['mensagem'] = 'VPN em modo MANUAL';
		    $this->db->links->sqlBuilder->update($link, 'links');
		    $this->db->links->sqlBuilder->where('id = '.$campo);
		    return $this->db->links->sqlBuilder->executeQuery();
		}
    }
    
    
	public function salvarLink($array){
		if($array['porta'] == ''){
		    $array['porta'] = 23;
		}
		if($array['timeout'] == ''){
		    $array['timeout'] = 1000;
		}
		$this->db->links->sqlBuilder->insert($array, 'links');
		return $this->db->links->sqlBuilder->executeQuery();
		
	}
    
    public function getLink($id){
	$this->db->links->sqlBuilder->select('*');
	$this->db->links->sqlBuilder->from('links');
	$this->db->links->sqlBuilder->where('id = '.$id);
	$return = $this->db->links->sqlBuilder->executeQuery();
	return $return[0];	
    }
    
    public function update($array){
		$id = $array['id'];
		unset($array['id']);
		//var_dump($array);
		//var_dump( array_keys($array));
		foreach($array as $item){
			echo $item;
		}
		$this->db->links->sqlBuilder->update($array, 'links');
		$this->db->links->sqlBuilder->where('"id" = '.$id);
		return $this->db->links->sqlBuilder->executeQuery();
    }
    
    public function delete($array){
		$id = $array['id'];
		$this->db->links->sqlBuilder->delete();
		$this->db->links->sqlBuilder->from('links');
		$this->db->links->sqlBuilder->where('"id" = '.$id);
		return $this->db->links->sqlBuilder->executeQuery();
    }
    
    
    public function ativarVPN($id){
		$link['statusVPN'] = 1;
		$this->db->links->sqlBuilder->update($link, 'links');
		$this->db->links->sqlBuilder->where('"id" = '.$id);
		$this->db->links->sqlBuilder->executeQuery();
    }
    
    public function desativarVPN($id){
		$link['statusVPN'] = 0;
		$this->db->links->sqlBuilder->update($link, 'links');
		$this->db->links->sqlBuilder->where('id = '.$id);
		$this->db->links->sqlBuilder->executeQuery();
    }
    
    public function setLinkToggle($id, $valor){
	if($valor == 'true'){
	    $link['statusLink'] = 1;
	    $this->db->links->sqlBuilder->update($link, 'links');
	    $this->db->links->sqlBuilder->where('id = '.$id);
	    $this->db->links->sqlBuilder->executeQuery();
	}else{
	    $link['statusLink'] = 0;
	    $this->db->links->sqlBuilder->update($link, 'links');
	    $this->db->links->sqlBuilder->where('id = '.$id);
	    $this->db->links->sqlBuilder->executeQuery();
	}
    }
    
    public function gravaTempo(){
    	$item = array();
    	$item['"ultimaVerificacao"'] = time();
    	$this->db->links->sqlBuilder->update($item, 'vpnAutoGeral');
		$this->db->links->sqlBuilder->executeQuery();
    }
    
    public function getTempo(){
		$this->db->links->sqlBuilder->select('"ultimaVerificacao"');
    	$this->db->links->sqlBuilder->from('vpnAutoGeral');
    	$return = $this->db->links->sqlBuilder->executeQuery();
		return $return[0]['ultimaVerificacao'];
    }
    
    
    
    
    ////LOGS LINKS-------------
  

	public function writeLogsLink($string, $user = null){
		if($user == null) $user = 'System';
		$data = array('"instant"' => date('Y/m/d H:i:s'),'"user"' => $user, '"mensagem"' => $string, '"ip"' => $_SERVER['REMOTE_ADDR']);
		$this->db->links->sqlBuilder->insert($data, 'log');
		return $this->db->links->sqlBuilder->executeQuery();
	}

	public function getLogsLink($page, $division, $search){
		$this->db->links->sqlBuilder->select('instant, titulo, "timeoutMin", "timeoutMid", "timeoutMax", links_log.mensagem ');
		$this->db->links->sqlBuilder->from('links_log');
		$this->db->links->sqlBuilder->join('links on links.id = links_log.id_link');
		$this->db->links->sqlBuilder->where("links.tipo_servico = 0 AND links_log.mensagem <> '' AND (upper(links_log.mensagem) LIKE upper('%$search%') OR upper(links_log.ip) LIKE upper('%$search%') OR upper(links.titulo) LIKE upper('%$search%'))");
		$this->db->links->sqlBuilder->orderby('links_log.id DESC');
		$this->db->links->sqlBuilder->limit($division);
		$this->db->links->sqlBuilder->offset(($page*$division)-$division);
		$return = $this->db->links->sqlBuilder->executeQuery();
		return $return;
	}
	
	public function countLogsLinks($search){
		$this->db->links->sqlBuilder->select('COUNT(*)');
		$this->db->links->sqlBuilder->from('links_log');
		$this->db->links->sqlBuilder->join('links on links.id = links_log.id_link');
		$this->db->links->sqlBuilder->where("links.tipo_servico = 0 AND links_log.mensagem <> '' AND (upper(links_log.mensagem) LIKE upper('%$search%') OR upper(links_log.ip) LIKE upper('%$search%') OR upper(links.titulo) LIKE upper('%$search%'))");
		$return = $this->db->links->sqlBuilder->executeQuery();
		return $return[0]['count'];
	}
	
	
	////LOGS SERVICOS-------------
  

	public function writeLogsServico($string, $user = null){
		if($user == null) $user = 'System';
		$data = array('instant' => date('Y/m/d H:i:s'),'user' => $user, 'mensagem' => $string, 'ip' => $_SERVER['REMOTE_ADDR']);
		$this->db->links->sqlBuilder->insert($data, 'log');
		echo $this->db->links->sqlBuilder->getQuery();
		//die();
		return $this->db->links->sqlBuilder->executeQuery();
	}

	public function getLogsServico($page, $division, $search){
		$this->db->links->sqlBuilder->select('instant, titulo, "timeoutMin", "timeoutMid", "timeoutMax", links_log.mensagem ');
		$this->db->links->sqlBuilder->from('links_log');
		$this->db->links->sqlBuilder->join('links on links.id = links_log.id_link');
		$this->db->links->sqlBuilder->where("links.tipo_servico = 1 AND links_log.mensagem <> '' AND (upper(links_log.mensagem) LIKE upper('%$search%') OR upper(links_log.ip) LIKE upper('%$search%') OR upper(links.titulo) LIKE upper('%$search%'))");
		$this->db->links->sqlBuilder->orderby('links_log.id DESC');
		$this->db->links->sqlBuilder->limit($division);
		$this->db->links->sqlBuilder->offset(($page*$division)-$division);
		
		return $this->db->links->sqlBuilder->executeQuery();
	}
	
	public function countLogsServico($search){
		$this->db->links->sqlBuilder->select('COUNT(*)');
		$this->db->links->sqlBuilder->from('links_log');
		$this->db->links->sqlBuilder->join('links on links.id = links_log.id_link');
		$this->db->links->sqlBuilder->where("links.tipo_servico = 1 AND links_log.mensagem <> '' AND (upper(links_log.mensagem) LIKE upper('%$search%') OR upper(links_log.ip) LIKE upper('%$search%') OR upper(links.titulo) LIKE upper('%$search%'))");
		$return = $this->db->links->sqlBuilder->executeQuery();
		return $return[0]['count'];
	}
    
    

}
