<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');


class ModelLog  extends ModularCore\Model{
	function __construct() {
		parent::__construct();

		$db = 'links';
		$dbname = 'sistema_links';
		ModularCore\core::$coreConfig['databases'][$db]['dbname'] = $dbname;
		$this->loadDb($db, $dbname);
	}

	public function write($string, $user = null){
		if($user == null) $user = 'System';
		$data = array('date' => date('Y/m/d H:i:s'),'user' => $user, 'action' => $string, 'ip' => $_SERVER['REMOTE_ADDR']);
		$this->db->sistema_links->sqlBuilder->insert($data, 'log');
		$return = $this->db->sistema_links->sqlBuilder->executeQuery();
		return $return;
	}

	public function get($page, $division, $search){
		$this->db->sistema_links->sqlBuilder->select('*');
		$this->db->sistema_links->sqlBuilder->from('log');
		$this->db->sistema_links->sqlBuilder->where("LOWER(user) LIKE LOWER('%$search%') OR LOWER(action) LIKE LOWER('%$search%') OR LOWER(ip) LIKE LOWER('%$search%')");
		$this->db->sistema_links->sqlBuilder->orderby('id DESC');
		$this->db->sistema_links->sqlBuilder->limit($division);
		$this->db->sistema_links->sqlBuilder->offset(($page*$division)-$division);
		$return = $this->db->sistema_links->sqlBuilder->executeQuery();
		return $return;
	}

	public function count($search){
		$this->db->sistema_links->sqlBuilder->select('COUNT(*)');
		$this->db->sistema_links->sqlBuilder->from('log');
		$this->db->sistema_links->sqlBuilder->where("LOWER(user) LIKE LOWER('%$search%') OR LOWER(action) LIKE LOWER('%$search%') OR LOWER(ip) LIKE LOWER('%$search%')");
		$return = $this->db->sistema_links->sqlBuilder->executeQuery();
		return $return[0]['count'];
	}
}
