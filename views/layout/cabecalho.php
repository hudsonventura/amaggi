<div class="col-md-12">
		    
			<div class="panel panel-default text-center">
			
				<h4><?php echo $title; ?></h4>
				
				<!--Fancy Button-->
				<div class="[ form-group ]">
					<input type="checkbox" name="fancy-checkbox-geral" id="geral-checkbox" autocomplete="off" <?php if($geral['vpnAuto']){echo 'checked';}?> />
					<div class="[ btn-group ]">
						<label for="geral-checkbox" class="[ btn btn-primary ]">
							<span class="[ glyphicon glyphicon-ok ]"></span>
							<span> </span>
						</label>
						<label for="geral-checkbox" class="[ btn btn-primary active ]">
							VPN automático para todos os links
						</label>
					</div>
				</div>
				<!--Fancy Button-->
				
			</div>
					
		</div>