	</div>
    </div>



    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo ASSETS?>bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo ASSETS?>metisMenu/js/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo ASSETS?>datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo ASSETS?>datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo ASSETS?>sb-admin-2/js/sb-admin-2.js"></script>
    
    <!-- Morris Charts JavaScript -->
    <script src="<?php echo ASSETS?>raphael/raphael-min.js"></script>
    <script src="<?php echo ASSETS?>morrisjs/morris.min.js"></script>
    <script src="<?php echo ASSETS?>morrisjs/morris-data.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
