<?php require ('layout/head.php'); ?>
<?php require ('layout/header.php'); ?>
			
			<div class="row">
					<div class="pull-right">
						Última verificação de links em <?php echo $ultimaVerificacao ?>, há <?php echo $tempo ?> atrás
						<br /><br />
					</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<div class="panel panel-default">
						<div class="panel-heading">
							Desbloqueio de usuários
						</div>
						<div class="panel-body">
							<table class="table table-striped">
								<?php foreach($relatorioUsuarios as $key => $value){ ?>
									<tr>
										<td><?php echo $key; ?></td>
										<td><?php echo $value; ?></td>
									</tr>
								<?php } ?>
							</table>
						</div>
					</div>
				</div>
				
				<?php //var_dump($teste); ?>
				
				<div class="col-md-3">
					<div class="panel panel-default">
						<div class="panel-heading">
							Ranking links offline
						</div>
						<div class="panel-body">
							<table class="table table-striped">
								<?php foreach($relatorioLinks as $key => $value){ ?>
									<tr>
										<td><?php echo $key; ?></td>
										<td><?php echo $value; ?></td>
									</tr>
								<?php } ?>
							</table>
						</div>
					</div>
				</div>
				
				<div class="col-md-3">
					<div class="panel panel-danger">
						<div class="panel-heading">
							Links offline
						</div>
						<div class="panel-body">
							<table class="table table-striped">
								<?php foreach($relatorioOffline as $key => $value){ ?>
									<tr>
										<td><?php echo $key; ?></td>
									</tr>
								<?php } ?>
							</table>
						</div>
					</div>
				</div>
				
				<div class="col-md-3">
					<div class="panel panel-default">
						<div class="panel-heading">
							VPNs ativas
						</div>
						<div class="panel-body">
							<table class="table table-striped">
								<?php foreach($relatorioVPN as $key => $value){ ?>
									<tr>
										<td><?php echo $key; ?></td>
									</tr>
								<?php } ?>
							</table>
						</div>
					</div>
				</div>
			</div>
<?php require ('layout/footer.php'); ?>