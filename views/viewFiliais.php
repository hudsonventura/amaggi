<?php require ('layout/head.php'); ?>
<body>

    <div id="wrapper">



		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header"><?php echo $title?></h1>
				</div>
			</div>




<h4 class="text-center">
	<?php echo $this->coreView['ultimaVerificacao']; ?>
</h4>
<div class="row">
	<div class="text-center" id="contador"></div>

</div>
<table class="table table-striped">
    <thead>
	<th>Localidade</th>
	<th>IP</th>
	<th>Estado do link primário</th>
	<th>Utilizando link secundário</th>
	<th>Mensagem</th>
    </thead>
    <tbody>
	<?php foreach ($listaLinks as $link) {?>
	<tr>
	<td><?php echo $link['titulo']?></td>
	<td><?php echo $link['ip_router']?></td>
	
	<td>
	    <input class="form-control" type="checkbox" name="<?php echo $link['id'] ?>-checkboxLink" />
	    <script>$('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('state', '<?php if($link['statusLink']){echo 'on';} ?>' );
			    $('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('offColor', 'danger');
			    $('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('readonly', 'true');
			    $('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('size', 'mini');</script>
	</td>
	
	<td>
	    <input class="form-control" type="checkbox" name="<?php echo $link['id'] ?>-checkboxVPN" id="<?php echo $link['id'] ?>-checkboxVPN" />
	    <script>
		 $('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('state','<?php if($link['statusVPN']){echo 'on';} ?>');
		$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('readonly', true);
		$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('onColor', 'danger');
		$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('offColor', 'primary');
		$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('size', 'mini');</script>
	</td>
	

	<td id="mensagem-<?php echo $link['id']?>"><?php echo $link['mensagem']?></td>
	</tr>
	

	<?php }?>
    </tbody>
</table>







<?php require ('layout/footer.php'); ?>