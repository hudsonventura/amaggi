<?php require ('layout/head.php'); ?>
<?php require ('layout/header.php'); ?>


		<div class="container col-xs-12" role="tabpanel" aria-labelledby="monitor" aria-expanded="true">
			    <div class="panel panel-default">
				<div class="panel-body">
				    <form method="POST">
					<?php if (isset($link)){echo '<input type="text" name="id" id="id" hidden="" value="'.$link['id'].'">';}?>
					
					<div class="col-md-4">
						<div class="form-group">
						    <label for="titulo">Título</label>
						    <input type="text" class="form-control" name="titulo" id="titulo" placeholder="Título" value="<?php if (isset($link)){echo $link['titulo'];}?>">
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="form-group">
						    <label for="ip_router">IP do Roteador</label>
						    <input type="text" class="form-control" name="ip_router" id="ip_router" placeholder="IP do Roteador" value="<?php if (isset($link)){echo $link['ip_router'];}?>">
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="form-group">
						    <label for="porta">Porta do Roteador</label>
						    <input type="text" class="form-control" name="porta" id="porta" placeholder="23" value="<?php if (isset($link)){echo $link['porta'];}?>">
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="form-group">
						    <label for="timeout">Timeout</label>
						    <input type="text" class="form-control" name="timeout" id="timeout" placeholder="1000" value="<?php if (isset($link)){echo $link['timeout'];}?>">
						</div>
					</div>
					
					<div class="col-md-2">
						<label>Tipo de Comutação</label>
						<div class="radio">
							<label>
								<input type="radio" name="tipo_virada" id="tipo_virada" value="0" <?php if (isset($link) && $link['tipo_virada']==0){echo 'checked=""';}?> /> Normal
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="tipo_virada" id="tipo_virada" value="1" <?php if (isset($link) && $link['tipo_virada']==1){echo 'checked=""';}?> /> Inversa
							</label>
						</div>
					</div>
					
					
					<div class="form-group">
					    <label for="cmd_ativaVPN">Comandos para ATIVAR a VPN</label>
					    <textarea class="form-control" style="resize: vertical; height: 150px;" name="cmd_ativaVPN" id="cmd_ativaVPN" placeholder="Dividir os comandos por |" ><?php if (isset($link)){echo $link['cmd_ativaVPN'];}?></textarea>
					</div>
					
					<div class="form-group">
					    <label for="cmd_desativaVPN">Comandos para DESATIVAR a VPN</label>
					    <textarea class="form-control" style="resize: vertical; height: 150px;" name="cmd_desativaVPN" id="cmd_desativaVPN" placeholder="Dividir os comandos por |" ><?php if (isset($link)){echo $link['cmd_desativaVPN'];}?></textarea>
					</div>

					
					
					
					
				    <button type="submit" class="btn btn-default">Salvar</button>
				</form>
				</div>
			    </div>
                        </div>

<?php require ('layout/footer.php'); ?>