﻿<?php require ('layout/head.php'); ?>
<?php require ('layout/header.php'); ?>


<div class="pull-right">
	<?php echo $ultimaVerificacao; ?> <a href="<?php echo BASEDIR?>php_errors.log">Logs</a>
</div>
<div class="row">
	<div class="text-center" id="contador"></div>

</div>
<h3>Monitoramento VPN</h3>
<table class="table table-striped">
    <thead>
	<th>Localidade</th>
	<th>IP</th>
	<th>Status Link VPN</th>
	<th>Status Comutação VPN</th>
	<th>Automático</th>
	<th>Mensagem</th>
    </thead>
    <tbody>
    	<?php  foreach ($listaLinks as $link) {
			if($link['tipo_virada']==1) { //TIPO VIRADA INVERSA
				if ($link['tipo_servico'] == 0) {?>
		<tr>
		<td><?php echo $link['titulo']?></td>
		<td><?php echo $link['ip_router']?></td>
		
		<td>
		    <input class="form-control" type="checkbox" name="<?php echo $link['id'] ?>-checkboxLink" />
		    <script>$('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('state', '<?php if($link['statusLink']){echo 'on';} ?>' );
				    $('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('offColor', 'danger');
				    $('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('readonly', 'true');
				    $('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('size', 'mini');</script> 
	
		</td>
		
		<td>
			<?php //CASO SEJA UMA COMUTAÇÃO INVERSA ?><input class="form-control" type="checkbox" name="<?php echo $link['id'] ?>-checkboxVPN" id="<?php echo $link['id'] ?>-checkboxVPN">
		    <script>$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('state','<?php if($link['statusVPN']){echo 'on';} ?>');
			$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('readonly', <?php if($link['vpnAuto']){echo 'true';}else{echo 'false';} ?>);
			$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('offColor', 'danger');
			$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('onText', 'VPN');
			$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('offText', 'MPLS');
			$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('onColor', 'primary');
			$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('size', 'mini');</script> 
			
		</td>
		
		<td>
		    <div class="[ form-group ]">
			<input type="checkbox" name="fancy-checkbox-<?php echo $link['id'] ?>" id="fancy-checkbox-<?php echo $link['id'] ?>" autocomplete="off" <?php if($link['vpnAuto']){echo 'checked';}?>/>
			<div class="[ btn-group ]">
				<label for="fancy-checkbox-<?php echo $link['id'] ?>" class="[ btn btn-primary btn-xs ]">
					<span class="[ glyphicon glyphicon-ok ]"></span>
					<span> </span>
				</label>
	<!--			<label for="fancy-checkbox-<?php //echo $link['id'] ?>" class="[ btn btn-primary btn-xs ]">
					VPN Automático
				</label>-->
			</div>
		    </div>
		</td>
		<td id="mensagem-<?php echo $link['id']?>"><?php echo $link['mensagem']?></td>
		</tr>
		
		<script>				
	
	
								//ajax que enviar os dados para salvar as opções INDIVIDUAIS
								$("#fancy-checkbox-<?php echo $link['id'] ?>").on('change', function(e) {
									var json = {"campo": "<?php echo $link['id'] ?>",
												"valor": $("#fancy-checkbox-<?php echo $link['id'] ?>").prop('checked'),
												"sender": "<?php if(isset($usuario['samaccountname'][0])){echo $usuario['samaccountname'][0];}else{echo 'SISTEMA DE LINKS';} ?>"};
									$.ajax({
										type: "GET",
										url: "<?php echo MODULEDIR?>/ajax/setOpcoes",
										data: {'json':json},
										success: function (data) {
											//alert(data);
											if(data == 1){
											    $('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('readonly', true);
											}else{
											    $('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('readonly', false);
											}
											
										}
									});
								});
								//ajax que enviar os dados para salvar as opções INDIVIDUAIS
	
	
	
	
	
								//ajax que comuta a VPN
								$("#<?php echo $link['id'] ?>-checkboxVPN").on('switchChange.bootstrapSwitch', function(event, state) {
									$('#modalVPN').modal('show');
									var json = {"campo": "<?php echo $link['id'] ?>",
												"valor": $("#<?php echo $link['id'] ?>-checkboxVPN").bootstrapSwitch('state'),
												"sender": "<?php if(isset($usuario['samaccountname'][0])){echo $usuario['samaccountname'][0];}else{echo 'SISTEMA DE LINKS';} ?>"};
									$.ajax({
										type: "GET",
										url: "<?php echo MODULEDIR?>/ajax/comutaVPN",
										data: {'json':json},
										success: function (data) {
											console.log(data);
											var obj = JSON.parse(data);
											//alert(obj[0]); alert(obj[1]);
											console.log(obj);
											$('#modalVPN .bootbox-body').html('<h4>'+obj[0]+'<h4>');
											$('#mensagem-<?php echo $link['id']?>').html(obj[0]);
											$('#modalVPN').modal('show');
										}
									});
								});
								
	
	
		</script>
		<?php }?>
	<?php }
		}?>
        
    </tbody>
</table>
<hr />
<h3>Monitoramento MPLS</h3>
<table class="table table-striped">
    <thead>
	<th>Localidade</th>
	<th>IP</th>
	<th>Status Link MPLS</th>
	<th>Status Comutação VPN</th>
	<th>Automático</th>
	<th>Mensagem</th>
    </thead>
    <tbody>
	<?php  foreach ($listaLinks as $link) {
			if($link['tipo_virada']==0) { //TIPO VIRADA NORMAL COMUM
				if ($link['tipo_servico'] == 0) {?>
		<tr>
		<td><?php echo $link['titulo']?></td>
		<td><?php echo $link['ip_router']?></td>
		
		<td>
		    <input class="form-control" type="checkbox" name="<?php echo $link['id'] ?>-checkboxLink" />
		    <script>$('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('state', '<?php if($link['statusLink']){echo 'on';} ?>' );
				    $('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('offColor', 'danger');
				    $('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('readonly', 'true');
				    $('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('size', 'mini');</script> 
	
		</td>
		
		<td>
		    	<?php //CASO SEJA UMA COMUTAÇÃO COMUM ?><input class="form-control" type="checkbox" name="<?php echo $link['id'] ?>-checkboxVPN" id="<?php echo $link['id'] ?>-checkboxVPN">
		    <script>$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('state','<?php if($link['statusVPN']){echo 'on';} ?>');
			$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('readonly', <?php if($link['vpnAuto']){echo 'true';}else{echo 'false';} ?>);
			$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('onColor', 'danger');
			$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('offColor', 'primary');
			$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('onText', 'VPN');
			$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('offText', 'MPLS');
			$('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('size', 'mini');</script>
			
		</td>
		
		<td>
		    <div class="[ form-group ]">
			<input type="checkbox" name="fancy-checkbox-<?php echo $link['id'] ?>" id="fancy-checkbox-<?php echo $link['id'] ?>" autocomplete="off" <?php if($link['vpnAuto']){echo 'checked';}?>/>
			<div class="[ btn-group ]">
				<label for="fancy-checkbox-<?php echo $link['id'] ?>" class="[ btn btn-primary btn-xs ]">
					<span class="[ glyphicon glyphicon-ok ]"></span>
					<span> </span>
				</label>
	<!--			<label for="fancy-checkbox-<?php //echo $link['id'] ?>" class="[ btn btn-primary btn-xs ]">
					VPN Automático
				</label>-->
			</div>
		    </div>
		</td>
		<td id="mensagem-<?php echo $link['id']?>"><?php echo $link['mensagem']?></td>
		</tr>
		
		<script>				
	
	
								//ajax que enviar os dados para salvar as opções INDIVIDUAIS
								$("#fancy-checkbox-<?php echo $link['id'] ?>").on('change', function(e) {
									var json = {"campo": "<?php echo $link['id'] ?>",
												"valor": $("#fancy-checkbox-<?php echo $link['id'] ?>").prop('checked'),
												"sender": "<?php if(isset($usuario['samaccountname'][0])){echo $usuario['samaccountname'][0];}else{echo 'SISTEMA DE LINKS';} ?>"};
									$.ajax({
										type: "GET",
										url: "<?php echo MODULEDIR?>/ajax/setOpcoes",
										data: {'json':json},
										success: function (data) {
											console.log(data);
											if(data == 1){
											    $('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('readonly', true);
											}else{
											    $('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('readonly', false);
											}
											
										}
									});
								});
								//ajax que enviar os dados para salvar as opções INDIVIDUAIS
	
	
	
	
	
								//ajax que comuta a VPN
								$("#<?php echo $link['id'] ?>-checkboxVPN").on('switchChange.bootstrapSwitch', function(event, state) {
									$('#modalVPN').modal('show');
									var json = {"campo": "<?php echo $link['id'] ?>",
												"valor": $("#<?php echo $link['id'] ?>-checkboxVPN").bootstrapSwitch('state'),
												"sender": "<?php if(isset($usuario['samaccountname'][0])){echo $usuario['samaccountname'][0];}else{echo 'SISTEMA DE LINKS';} ?>"};
									$.ajax({
										type: "GET",
										url: "<?php echo MODULEDIR?>/ajax/comutaVPN",
										data: {'json':json},
										success: function (data) {
											//alert(data);
											var obj = JSON.parse(data);
											console.log(obj);
											
											$('#modalVPN .bootbox-body').html('<h4>'+obj[0]+'<h4>');
											
											$('#mensagem-<?php echo $link['id']?>').html(obj[0]);
											$('#modalVPN').modal('show');
										}
									});
								});
								
	
	
		</script>
		<?php }?>
	<?php }
		}?>
    </tbody>
</table>






<?php if(isset($admin)){?>
<script>
	
	<?php foreach ($listaLinks as $link) {?>
	    $("#<?php echo $link['id'] ?>-verificacao").click();
	    setTimeout('contar('+refresh+')', 1000);
	<?php }?>
	
	var refresh = new Number();
    refresh =300; // tempo de refresh logo após fazer a verificação dos links
    setTimeout("location.reload(true);",refresh*1000);

</script>
<?php }?>




	<!-- MODAL VPN-->
	<div id="modalVPN" class="modal fade fv-modal-stack" style="z-index: 9999;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<h5 class="panel-title">Confirmação</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<div class="bootbox-body">
						
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
	<!-- MODAL VPN-->


	
	









<?php require ('layout/footer.php'); ?>