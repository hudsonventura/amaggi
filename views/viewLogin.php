<?php require ('layout/head.php'); ?>



<body>

    <div class="container">
        <div class="row">
			<div class="col-md-4">
				<div class="login-panel panel text-center">
					<div class="panel-heading" style="background-color: #fff;">
						<img src="<?php echo ASSETS?>logo_csc.png" width="228px" />
					</div>
					<div class="panel-body">
					Sistema de Links - Acesso apenas para a TI
                        <form class="form-signin center" role="form" method="POST">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Usuário de Rede" name="login" type="text" autofocus="" />
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Senha" name="pass" type="password" />
                                </div>
				
				
				
								<div class="">
									<?php if(isset($msgLogin)){?>
										<div class="alert alert-<?php echo $msgType;?>"><?php echo $msgLogin;?></div>
									<?php }?>
								</div>
								<br />
								<br />
								<button class="btn btn-lg btn-primary btn-block" style="background-color: #0072c6;" type="submit">Entrar</button>
							</fieldset>
							
						</form>
						<div class="m-t-20 m-b-40 p-b-40">
                            <a href="<?php echo MODULEDIR?>/desbloqueio" class="text-muted pull-left"><i class="fa fa-arrow-left"></i> Desbloqueio</a>
                        </div>
					</div>
				</div>
			</div>

        </div>
    </div>


<?php require ('layout/footer.php'); ?>




