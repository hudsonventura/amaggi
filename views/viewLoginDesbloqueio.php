<?php require ('layout/head.php'); ?>



<body>

    <div class="container">
        <div class="row">
		    <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel text-center">
                    <div class="panel-heading">
						<img src="<?php echo ASSETS ?>/logo_csc.png" width="95%" />
                    </div>
                    <div class="panel-body">
						<br />
                    	Desbloqueio de usuário de rede e email<br><small>Digite seu CPF no campo abaixo para desbloquear seu usuário de rede</small>
                        <form class="form-signin center" role="form" method="POST">
                            <fieldset>
				
				
                                <div class="form-group">
                                    <input class="form-control" name="cpf" type="password" autofocus="" placeholder="Digite seu CPF para desbloquear" />
                                </div>
				
				

				
				
                                <div class="">
                                    <?php if(isset($msgDesbloqueio)){?>
										<div class="alert alert-<?php echo $msgType;?>"><?php echo $msgDesbloqueio;?></div>
									<?php }?>
                                </div>
                                
                                

				

								<br />
								<button class="btn btn-lg btn-primary btn-block" style="background-color: #0072c6;" type="submit">Desbloquear</button>
                            </fieldset>
                        </form>
                        
                         <div class="m-t-20 m-b-40 p-b-40">
                            <a href="<?php echo 'https://'.$_SERVER['SERVER_NAME'].MODULEDIR?>/login" class="text-muted pull-right">Fazer login <i class="fa fa-arrow-right"></i></a>
                        </div>
			
                    </div>
                </div>
            </div>
	    
	    
	    
	    



<?php require ('layout/footer.php'); ?>




