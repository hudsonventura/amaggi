<?php require ('layout/head.php'); ?>
<?php require ('layout/header.php'); ?>
	
	<?php if(isset($msgLog)){ ?>
		<div class="alert alert-<?php echo $msgLogType ?>">
			<?php echo $msgLog; ?>
		</div>
	<?php } ?>
	
	<div class="pull-right">
		<form action="" method="GET">
			<div class="input-group">
				<input class="form-control" id="busca" name="busca" placeholder="Busca" />
				<span class="input-group-btn"><button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button></span>
			</div>
		</form>
	</div>
	
	
	
	
    <?php echo $logs?>
    
    <div class="text-center">
		<?php echo $pagination?>
	</div>



<?php require ('layout/footer.php'); ?>