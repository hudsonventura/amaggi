<?php require ('layout/head.php'); ?>
<?php //require ('layout/header.php'); ?>

<!-- pickadate.js  -->
<script src="<?php echo ASSETS;?>pickadate/picker.js"></script>
<!-- pickadate.js  -->
<script src="<?php echo ASSETS;?>pickadate/picker.date.js"></script>
<!-- pickadate.js  -->
<link href="<?php echo ASSETS;?>pickadate\themes/default.css" rel="stylesheet" />
<!-- pickadate.js  -->
<link href="<?php echo ASSETS;?>pickadate\themes/default.date.css" rel="stylesheet" />



<?php // matriz de entrada
$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','-','(',')',',',';',':','|','!','"','#','$','%','&','/','=','?','~','^','>','<','ª','º' );

// matriz de saída
$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','I','O','U','n','n','c','C','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_' );
?>

			<script>
				var salaJaReservada = '<i style="color: #F00"><b>SALA JÁ RESERVADA</b></i>';
					function reservarSala(string, botao){
						if(botao.innerHTML == 'Reservar'){
							$.ajax({
								type: "GET",
								url: "<?php echo CONTROLLERDIR?>",
								data: {	"reservar" : string},
								success: function (data) {
									var obj = JSON.parse(data);
									//alert(obj);
									if(obj == true){
										var id = botao.id;
										botao.innerHTML = '<i style="color: #11AA11"><b><?php echo $usuario['samaccountname'][0]; ?></b></i>';
										document.getElementById(id.substr(0, 8)+'_'+id.substr(15)).setAttribute("disabled","disabled");
									}else{
										botao.innerHTML = salaJaReservada;
										botão.disabled = true;
										document.getElementById(id.substr(0, 8)+'_'+id.substr(15)).setAttribute("disabled","disabled");
									}
								}
							});
						}
						if(botao.innerHTML != 'Reservar' && botao.innerHTML != salaJaReservada){
							$.ajax({
								type: "GET",
								url: "<?php echo CONTROLLERDIR?>",
								data: {	"liberar" : string},
								success: function (data) {
									//var obj = JSON.parse(data);
									if(data == '1' || data == 1){
										//alert('Liberiou a sala');
										botao.innerHTML = 'Sala Livre';
									}
								}
							});
						}

					}

					function retornaSalaLivre(botao){
						//alert(botao.innerHTML);
						if(botao.innerHTML == 'Reservar'){
							//alert('ok');
							botao.innerHTML= "Sala Livre";
						}
					}

					function retornaSalaReserva(botao){
						//alert(botao.innerHTML);
						if(botao.innerHTML == 'Sala Livre'){
							//alert('ok');
							botao.innerHTML= "Reservar";
						}
					}



			</script>
			
				 <!--<ul class="nav nav-tabs">
		  			<li class="active"><a data-toggle="tab" href="#2piso">Salas 2º Piso</a></li>
					<!--li><a data-toggle="tab" href="#atendimento">1º Piso - Salas de Atendimento</a></!--li>-->
		  			<!--<li><a data-toggle="tab" href="1º Piso">2º Piso - Salas de Reunião</a></li>-->
		  			<!--<li><a data-toggle="tab" href="#3">3º Piso - Salas de Reunião</a></li>-->
		  			<!--<li><a data-toggle="tab" href="#4">4º Piso - Salas de Reunião</a></li>-->
				<!--</ul>-->
				
				<div class="tab-content" style="margin-top: -30px;">
					<div class="col-md-2">
						<div class="panel panel-default">
							<div class="panel-heading">Datas</div>
							<div class="panel-body">
								<ul class="list-group">
									<?php	foreach($dias as $dia){
													if($dia['link'] == $data){
														echo "<li class='list-group-item ".$dia['class']."'>".$dia['view']."</li>";
													}else{
														echo "<a href='".CONTROLLERDIR."?dia=".$dia['link']."' class='list-group-item ".$dia['class']."' style='text-decoration: none; height: 30px; padding-top: 3px;'>".$dia['view']."</a>";
													}
												}
                                    ?>
									<li class='list-group-item'>



										<!--DIA-->
										<div class="form-group">
											<label>Outras Datas</label>
											<div class="input-group">
												<div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
												<input class="form-control dia" id="dia" name="dia"" value="<?php if(isset($dataCorrente)) echo $dataCorrente;?>" placeholder="Clique" />
											</div>
										</div>

										<script>
											$('.dia').pickadate({
											format: 'dd/mm/yyyy',
											//min: [<?php //echo date('Y').','.date('m').','.date('d')?>],
											disable: [1, 7],
											today: 'Hoje',
											clear: 'Limpar',
											close: 'Fechar',
											closeOnSelect: false,
											weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
											monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],

											onSet: function(context){
												var date = new Date(context['select']);
												var year    = date.getFullYear();
												var month   = date.getMonth()+1;
												month = ("0" + month).slice(-2);
												var day     = date.getDate();
												day = ("0" + day).slice(-2);
												console.log("?dia="+day+'-'+month+'-'+year);
											    var link = "?dia="+day+'-'+month+'-'+year;
											    if(link != '?dia=aN-aN-NaN'){
													window.location.href="<?php echo CONTROLLERDIR;?>"+link;
											    }
											},
										})
										</script>
										<!--DIA-->





									</li>

								</ul>
							</div>
						</div>
					</div>

                    <style>
                        .table-striped > tbody > tr:nth-of-type(2n+1) {
                            background-color: #e2e2e2;
                        }
                        .btn {
                            height: 20px;
							padding: 4px;
							padding-top: 0;
                        }

					</style>


					<!-- Reuniao -->
					<div id="2piso" class="tab-pane fade in active">
						<div class="col-md-3">
							<table class="table table-striped table-condensed">
								<thead style="font-size: 12px" class="center" width: 400px;>
									<tr>
										<td width="300px" style="width: 300px; color: #aaa; font-size: 14px; padding-top: 15px"><i style="color:#fff">.......................................</i><br />Horários da Manhã</td>
										<?php foreach($salas as $sala){
														if(strpos($sala['nome'],'Atend')=== false){
															echo '<th style="text-align: center;">'.$sala['nome'];
															//reserva em massa
															/*if( !$data|| $data < date('d-m-Y')){
																if($reservas){
																	foreach($reservas as $reserva){
																		if($reserva['nome'] == $sala['nome']){ //date('d-m-Y', strtotime(str_replace('/', '-',  substr($reserva['horario'], 0, 10)))) == date('y-m-d', strtotime(str_replace('/', '-', $data)))
																			$button = '<a href="#" class="btn btn-default" disabled ><i class="fa fa-level-down"></i> Reservar</a>';
																			goto pular;
																		}else{
																			$button = '<a id="'.$data.'_'.$sala['nome'].'" href="'.CONTROLLERDIR.'?reservarDia='.$data.'_'.$sala['nome'].'_'.$usuario['samaccountname'][0].'" class="btn btn-default"><i class="fa fa-level-down"></i> Reservar</a>';
																		}
																	}

																}else{
																	$button = '<a id="'.$data.'_'.$sala['nome'].'" href="'.CONTROLLERDIR.'?reservarDia='.$data.'_'.$sala['nome'].'_'.$usuario['samaccountname'][0].'" class="btn btn-default"><i class="fa fa-level-down"></i> Reservar</a>';
																}
																pular:
																echo $button;
															}*/
															echo'</th>';
														}
												}
										?>
									</tr>
								</thead>
								<tbody>
									

										<?php foreach($tabela as $key => $sala){
												  $date1 = new DateTime('2001-01-01 '.substr($key, 0, 5));
												  $date2 = new DateTime($date1->format('Y-m-d H:i:s'));
												  $date2->add(new DateInterval('PT30M'));


												  //trata horario do almoço
												  $colunaHorario = $date1->format('H:i').' - '. $date2->format('H:i');
												  if($colunaHorario == '11:30 - 12:00' || $colunaHorario == '12:00 - 12:30' || $colunaHorario == '12:30 - 13:00'){
													  if($colunaHorario == '11:30 - 12:00'){
														  	echo '<tr style="background-color: #fff; height: 20px"><td width="300px" style="width: 300px; color: #aaa"><i style="color:#fff">.......................................</i><br />Horários da Tarde</td>';
															  foreach ($salas as $sala)
															  {
																  echo '<td style="background-color: #fff;"></td>';
															  }

													  }
													  goto escapaHorarioAlmoço;

												  }else{ //horario comum
													  echo '<tr><td style="width: 100px;">'.$colunaHorario.'</td>';
												  }


													foreach($sala as $key2 => $horario){
														if(strpos($key2,'Atend')=== false){
															if(strpos($horario, 'Sala Livre')===false){
																foreach($usuario['memberof'] as $item) {
																	//var_dump($usuario['memberof']); die();
																	if($item == 'CN=csc.processo,OU=Email,OU=Grupos,OU=Servicos,DC=maggi,DC=corp' || $item == 'CN=apoio.administrativo,OU=Email,OU=Grupos,OU=Servicos,DC=maggi,DC=corp')
																	{
																		//ADMIN
																		//$botao =  '<td><a href="'.CONTROLLERDIR.'?liberar='.$data.'_'.$key.'_'.$key2.'_'.$usuario['samaccountname'][0].'" class="btn btn-default">'.$horario.'</a></td>';
																		goto autorizado;
																	}
							    									else{
																		//HORARIO ALHEIO
							    										$botao = '<td align="center"><a ID="'.str_replace($what, $by, $data.'_'.$key.'_'.$key2).'" class="btn btn-default">'.$horario.'</a></td>';
																	}
				    											}
				    											autorizado:

																if(isset($usuario['memberof'])){
																	foreach($usuario['memberof'] as $item) {
																		//var_dump();
																		if(strpos($item, 'CN=csc.processo,OU=Email,OU=Grupos,OU=Servicos,DC=maggi,DC=corp')){
																			//VER
																			$botao =  '<td style="align:center"><a id="'.str_replace($what, $by, $data.'_'.$key.'_'.$key2).'" href="'.CONTROLLERDIR.'?liberar='.$data.'_'.$key.'_'.$key2.'_'.$usuario['samaccountname'][0].'" class="btn btn-default">'.$horario.'</a></td>';
																		}
																	}
																}
															}else{
																//HORARIOS LIVRES
																$botao =  '<td align="center"><a id="'.$data.'_'.$key.'_'.$key2.'" class="btn btn-default" onmouseover="retornaSalaReserva(this)" " onClick="reservarSala(\''.$data.'_'.$key.'_'.$key2.'_'.$usuario['samaccountname'][0].'\', this)" onmouseout="retornaSalaLivre(this)">'.$horario.'</a></td>
																			';
															}
															if(strpos($horario, $usuario['samaccountname'][0])){
																//MEUS HORARIOS RESERVADOS
																$botao =  '<td align="center"><a id="'.$data.'_'.$key.'_'.$key2.'" class="btn btn-default" onmouseover="retornaSalaReserva(this)" onClick="reservarSala(\''.$data.'_'.$key.'_'.$key2.'_'.$usuario['samaccountname'][0].'\', this)" onmouseout="retornaSalaLivre(this)">'.$horario.'</a></td>';
															}
															

															//horarios passados
															$dataHoje = strtotime(date('d-m-Y'));
															$dataCorrente =  explode('-', $data);
															$dataCorrente = mktime(0, 0, 0, $dataCorrente[1], $dataCorrente[0], $dataCorrente[2]);
															if($dataHoje >= $dataCorrente){
																if($dataHoje > $dataCorrente){
																	if($horario <> 'Sala Livre'){ //Remove a formatação de cores caso o passado tenha sido reservado
																		$inicio = strpos($horario, '>')+1;
																		$fim = strpos($horario, '</');
																		$horario = substr($horario, $inicio, $fim-$inicio);
																	}
																	$botao = '<td align="center"><a class="btn btn-default"><i style="color: #BBB">--- '.$horario.' ---</i></a></td>';
																}
																if($dataHoje == $dataCorrente){
																	if($key < date('H:i:s')){
																		if($horario <> 'Sala Livre'){ //Remove a formatação de cores caso o passado tenha sido reservado
																			$inicio = strpos($horario, '>')+1;
																			$fim = strpos($horario, '</');
																			$horario = substr($horario, $inicio, $fim-$inicio);
																		}
																		$botao = '<td align="center"><a class="btn btn-default"><i style="color: #BBB">--- '.$horario.' ---</i></a></td>';
																	}
																}
															}

				    										echo $botao;
    													}


													}
													escapaHorarioAlmoço:
													echo '</tr>';
												}
										?>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- Reuniao -->

					<!-- ATENDIMENTO -->
				
					<!-- ATENDIMENTO -->
				</div>
			
			
			
                
			
			
		
				
				
		
			
			<!-- MODAL -->
			<div id="modal" class="modal fade fv-modal-stack" style="z-index: 9999;">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<h5 class="panel-title">Confirmação</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<div class="bootbox-body">
									
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-default" data-dismiss="modal">Ok</button>
						</div>
					</div>
				</div>
			</div>
			<!-- MODAL -->


<?php require ('layout/footer.php'); ?>