﻿<?php require ('layout/head.php'); ?>
<?php require ('layout/header.php'); ?>


<div class="pull-right">
	<?php echo $ultimaVerificacao; ?>
</div>
<div class="row">
	<div class="text-center" id="contador"></div>

</div>
<table class="table table-striped">
    <thead>
	<th>Serviço</th>
	<th>IP</th>
	<th>Status</th>
	<th>Porta</th>
	<th>Timeout</th>
	<th>Mensagem</th>
    </thead>
    <tbody>
	<?php foreach ($listaLinks as $link) {?>
	<?php if ($link['tipo_servico'] == 1) {?>
	<tr>
	<td><?php echo $link['titulo']?></td>
	<td><?php echo $link['ip_router']?></td>
	
	<td>
	    <input class="form-control" type="checkbox" name="<?php echo $link['id'] ?>-checkboxLink" />
	    <script>$('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('state', '<?php if($link['statusLink']){echo 'on';} ?>' );
			    $('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('offColor', 'danger');
			    $('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('readonly', 'true');
			    $('input[name="<?php echo $link['id'] ?>-checkboxLink"]').bootstrapSwitch('size', 'mini');</script>
	</td>
	
	<td id="mensagem-<?php echo $link['porta']?>"><?php echo $link['porta']?></td>
	<td id="mensagem-<?php echo $link['timeout']?>"><?php echo $link['timeout']?></td>
	

	<!--<td id="<?php echo $link['id'] ?>-verificacao" style="cursor: pointer;"><span class="fa fa-refresh"></span></td>-->
	<td id="mensagem-<?php echo $link['id']?>"><?php echo $link['mensagem']?></td>
	</tr>
	
	<script>				


							//ajax que enviar os dados para salvar as opções INDIVIDUAIS
							$("#fancy-checkbox-<?php echo $link['id'] ?>").on('change', function(e) {
								var json = {"campo": "<?php echo $link['id'] ?>",
											"valor": $("#fancy-checkbox-<?php echo $link['id'] ?>").prop('checked'),
											"sender": "<?php if(isset($usuario['samaccountname'][0])){echo $usuario['samaccountname'][0];}else{echo 'SISTEMA DE LINKS';} ?>"};
								$.ajax({
									type: "GET",
									url: "<?php echo MODULEDIR?>/ajax/setOpcoes",
									data: {'json':json},
									success: function (data) {
										//alert(data);
										if(data == 1){
										    $('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('readonly', true);
										}else{
										    $('input[name="<?php echo $link['id'] ?>-checkboxVPN"]').bootstrapSwitch('readonly', false);
										}
										
									}
								});
							});
							//ajax que enviar os dados para salvar as opções INDIVIDUAIS





							//ajax que comuta a VPN
							$("#<?php echo $link['id'] ?>-checkboxVPN").on('switchChange.bootstrapSwitch', function(event, state) {
								$('#modalVPN').modal('show');
								var json = {"campo": "<?php echo $link['id'] ?>",
											"valor": $("#<?php echo $link['id'] ?>-checkboxVPN").bootstrapSwitch('state'),
											"sender": "<?php if(isset($usuario['samaccountname'][0])){echo $usuario['samaccountname'][0];}else{echo 'SISTEMA DE LINKS';} ?>"};
								$.ajax({
									type: "GET",
									url: "<?php echo MODULEDIR?>/ajax/comutaVPN",
									data: {'json':json},
									success: function (data) {
										alert(data);
										var obj = JSON.parse(data);
										//alert(obj[0]); alert(obj[1]);
										
										$('#modalVPN .bootbox-body').html('<h4>'+obj[0]+'<h4>');
										$('#mensagem-<?php echo $link['id']?>').html(obj[0]);
										$('#modalVPN').modal('show');
									}
								});
							});
							


	</script>
	<?php }?>
	<?php }?>
    </tbody>
</table>



<?php if(isset($admin)){?>
<script>
	
	<?php foreach ($listaLinks as $link) {?>
	    $("#<?php echo $link['id'] ?>-verificacao").click();
	    setTimeout('contar('+refresh+')', 1000);
	<?php }?>
	
	var refresh = new Number();
    refresh =300; // tempo de refresh logo após fazer a verificação dos links
    setTimeout("location.reload(true);",refresh*1000);

</script>
<?php }?>




	<!-- MODAL VPN-->
	<div id="modalVPN" class="modal fade fv-modal-stack" style="z-index: 9999;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<h5 class="panel-title">Confirmação</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<div class="bootbox-body">
						
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
	<!-- MODAL VPN-->


	
	









<?php require ('layout/footer.php'); ?>