
<?php require ('layout/head.php'); ?>
<?php require ('layout/header.php'); ?>
			
		
<?php if($msg){?>
    <!--<div class="alert alert-<?php echo $msgType?>"><?php echo $msg?></div>-->
    
    			<script>
						$.notify({
							title: "<b>ATENÇÃO!</b><br>",
							message: "<?php echo $msg?>"
						},{
							type: "<?php echo $msgType?>",
							allow_dismiss: true,
							newest_on_top: false,
							timer: null,
						});

			</script>
<?php }?>


<?php if(strpos(FUNCTIONDIR, 'index'))
	{ ?>
		<div class="row col-md-9">
			<form class="form-inline" method="GET" action="">
			<div class="form-group">
			<input type="text" class="form-control" name="buscaUsuario" id="buscaUsuario" autofocus="" placeholder="Busca..." />
			</div>
	
			<div class="form-group">
				<select id="field" name="field" class="form-control">
				<option value="samaccountname">Buscar pelo nome de usuário</option>
				<option value="description">Buscar pelo Documento</option>
				<!--<option value="group">Buscar Grupo</option>-->
				</select>
			</div>
			<div class="form-group">
				<select id="domain" name="domain" class="form-control">
				<?php foreach($GLOBALS['coreConfig']['ActiveDirectory'] as $dominio => $ad){?>
					<option value="<?php echo $dominio?>"><?php echo $dominio.' - '.$ad['domain']?></option>
				<?php }?>
					<option value="all" selected=""><?php echo 'Todos os domínios'?></option>
				</select>
			</div>
	
			<button class="btn btn-default" type="submit" submit="">Buscar <i class="fa fa-search"></i></button>
			</form>
		</div>
<?php } ?>




 	
	<table class="table table-striped">
	    <thead>
		<tr>
		    <th>Usuário</th>
		    <th>Documento</th>
		    <th>Domínio</th>
		    <th>Status da Conta</th>
		    <th>Ações</th>
		</tr>
	    </thead>
	    <tbody>
				<?php foreach($listaUsuarios as $usuarioPorDominio){?>
					<?php if($usuarioPorDominio > 0){?>
						<?php foreach($usuarioPorDominio as $usuario){?>
							<?php if(isset($usuario['samaccountname'][0]) && $usuario['samaccountname'][0] <> 'Administrator' && $usuario['samaccountname'][0] <> 'Administrador' && $usuario['samaccountname'][0] <> 'Guest'){?>
								<!-- Filtro --><tr class="<?php if($usuario['useraccountcontrol'][0]<>512 && $usuario['useraccountcontrol'][0]<>544 && $usuario['useraccountcontrol'][0]<>66048 && $usuario['useraccountcontrol'][0]<>66080)echo 'danger alert-danger'?>">
								<!-- Usuario --><td><?php echo $usuario['samaccountname'][0];?></td>
								<!--Documento  --><td><?php if(isset($usuario['description'][0])){echo $usuario['description'][0];}?></td>
								<!-- Dominio --><td><?php if(isset($usuario['userprincipalname'])){echo $usuario['domain'];}?></td>
								<!--UserAccountControl  --><td ondblclick="window.open('<?php if(ModularCore\core::$coreConfig['environment'] <> 'PRD') echo MODULEDIR.'/usuarios/analisar?usuario='.$usuario['samaccountname'][0];?>')">
																												<?php 
																												$status = 'Admin';
																												if($usuario['useraccountcontrol'][0] == 512){$status = 'Normal';}
																												if($usuario['useraccountcontrol'][0] == 514){$status = 'Desligado';}
																												if($usuario['useraccountcontrol'][0] == 544){$status = 'Normal - Senha não requerida';}
																												if($usuario['useraccountcontrol'][0] == 66080){$status = 'Normal - Senha não requerida';}
																												if($usuario['useraccountcontrol'][0] == 546){$status = 'Desligado';}
																												if($usuario['useraccountcontrol'][0] == 66048){$status = 'Normal - Senha nunca expira';}
																												if($usuario['useraccountcontrol'][0] == 66050){$status = 'Bloqueado!';}
																												if(isset($usuario['badpwdcount']) && $usuario['badpwdcount'][0]==3){$status = 'Bloqueado!';}
																												echo $status;
																											?></td>
								<!-- Desbloqueio --><td><?php 
														
															$regraHabilitado = $usuario['useraccountcontrol'][0]<>514 && $usuario['useraccountcontrol'][0]<>546 && $usuario['useraccountcontrol'][0]<>66050;
															$email = (isset($usuario['mail'][0])) ? $usuario['mail'][0] : null ;
															
															$regraConsultor = !$regraHabilitado && ((isset($usuario['physicaldeliveryofficename'][0]) && strpos($usuario['physicaldeliveryofficename'][0], 'Consult') > -1) || strpos($usuario['description'][0], 'Consult') > -1 || ($email && strpos($email, 'maggi') == false && strpos($email, 'alm') == false && strpos($email, 'alz') == false) );
															//var_dump(strpos($usuario['mail'][0], 'maggi') == false ); die();
														
															if($regraHabilitado)
															{
																echo '<a class="btn btn-primary" style="margin: 2px;" href="'. MODULEDIR.'/usuarios/desbloquearUsuario?samaccountname='.$usuario['samaccountname'][0].'"><i class="fa fa-unlock"></i> Desbloquear ';
																echo '<a class="btn btn-warning" style="margin: 2px;" href="'. MODULEDIR.'/usuarios/resetSenha?samaccountname='.$usuario['samaccountname'][0].'"><i class="fa fa-key"></i> Reset de senha / Desbloquear';
															}else{
																if($usuario <> '1'){
																	echo '<a class="btn btn-danger" disabled="disabled" href="#"><i class="fa fa-thumbs-o-down"></i> Usuário Desativado';
																}
																	 
															}

															
							 								if($regraConsultor) {
																 echo '<a class="btn btn-success" style="margin: 2px;" title="Para ser considerado consultor, o usuario no AD deve ter o termo "Consultor" na descrição ou seu email não for @amaggi ou @aldzgraos ou @funda
																 			 href="'. MODULEDIR.'/usuarios/resetSenha?samaccountname='.$usuario['samaccountname'][0].'&domain='.substr($usuario['userprincipalname'][0], strpos($usuario['userprincipalname'][0], '@')+1).'"><i class="fa fa-thumbs-o-up"></i> Reabilitar Usuario Consultoria';
															}
															 
															 if($core['function'] == 'acessoGestor') //liberação de horas extras
															 { 
															 	//echo '<a class="btn btn-success" style="margin: 2px;" href="'. MODULEDIR.'/usuarios/horaExtra?samaccountname='.$usuario['samaccountname'][0].'&description='.$usuario['description'][0].'"><i class="fa fa-clock-o"></i> Liberar Horas Extras';
															 }
															 

														}?>
														
													</td>
								</tr>
							<?php }?>
						<?php }?>
					<?php }?>
				<?php 
					
					if(isset($listaGrupos)){
						foreach($listaGrupos as $grupo){
							var_dump($grupo);
						}
					}
				
				
				?>
	    </tbody>
	</table>

    

                       


<?php require ('layout/footer.php'); ?>